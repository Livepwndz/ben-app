package com.swifta.bj.mtn.fundamo.portal;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Slice;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.swifta.bj.mtn.fundamo.portal.bean.OutMerPacked;
import com.swifta.bj.mtn.fundamo.portal.bean.OutMerTest;
import com.swifta.bj.mtn.fundamo.portal.bean.OutSubRegPacked;
import com.swifta.bj.mtn.fundamo.portal.bean.OutSubPacked;
import com.swifta.bj.mtn.fundamo.portal.model.util.DateFormatFac;
import com.swifta.bj.mtn.fundamo.portal.model.util.Pager;
import com.swifta.bj.mtn.fundamo.portal.spring.config.Config;
import com.swifta.bj.mtn.fundamo.portal.spring.config.DataAccessConfigFundamo;
import com.swifta.bj.mtn.fundamo.portal.spring.config.JpaConfig;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.AccountIdentifier001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.CorporateAccountHolder001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.Entry001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.Person001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.RegistrationRequestData001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.Subscriber001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.Transaction001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.UserAccount001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo.Entry001Repo;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo.LedgerAccount001Repo;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo.Transaction001Repo;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo.Subscriber001Repo;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo.Transaction001Repo;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo.UserAccount001Repo;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { Config.class, DataAccessConfigFundamo.class,
		JpaConfig.class })
public class SpringFundamoTest {

	private static Logger log = LogManager.getLogger(SpringFundamoTest.class);
	
	
	@Autowired
	private ApplicationContext springAppContext;
	
	
	@Autowired
	private Transaction001Repo tRepo;
	
	@Autowired
	private Transaction001Repo subTRepo;
	
	@Autowired
	private Entry001Repo merRepo;
	
	
	
	
	@Autowired
	private Subscriber001Repo subRegRepo;
	
	@Autowired
	private UserAccount001Repo uAccRepo;
	
	
	
	@Autowired
	private LedgerAccount001Repo ledgerRepo;


	@Test
	@Ignore
	@Transactional
	public void testGetOneLedgerAccount() throws ParseException {
		Assert.assertNotNull("Fundamo ledger repo is null.", ledgerRepo);
		Page<Object[]> pages = ledgerRepo.getAllSumByDateRange(
				new Pager().getPageRequest(1),
				DateFormatFac.toDate("2011-06-10"),
				DateFormatFac.toDateUpperBound("2011-06-10"));
		Assert.assertNotEquals("No ledger record.", pages.getTotalPages(), 0);

		List<Object[]> records = pages.getContent();

		Iterator<Object[]> itr = records.iterator();
		while (itr.hasNext()) {
			Object[] record = itr.next();
			log.info("AccNo: " + record[0] + "Name: " + record[1] + "Amount: "
					+ record[2] + "Date: " + record[3], records);
		}

	}

	
	/*************************************** SUB. Transaction tests *******************/
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testSubPackedPageSliced() throws ParseException { 

		
		Assert.assertNotNull("Fundamo repo is null.", tRepo);
		Slice<OutSubPacked> pg = tRepo.findPageByDateRangePackedSliced(
				new Pager().getPageRequest(1), DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-03-08"));
		Assert.assertNotEquals("No transaction record.", pg.getNumberOfElements(), 0);
		
		
		Iterator<OutSubPacked> itr = pg.getContent().iterator();

		while (itr.hasNext()) {

			OutSubPacked transaction = itr.next();
			
			log.info( "T. Number: "+transaction.getTransactionNumber());
			log.info( "Type: "+transaction.getType());
			log.info( "Status: "+transaction.getStatus());
			log.info( "Amount: "+transaction.getAmount());
			log.info( "Payer MSISDN: "+transaction.getPayer());
			log.info( "Payee MSISDN: "+transaction.getPayee() );
			log.info( "Date: "+transaction.getDate());
			

		}
	} 
	
	
	
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testNewSubPackedPageSliced() throws ParseException { 
		
		Assert.assertNotNull("Fundamo repo is null.", subTRepo );
		Page<OutSubPacked> pg = subTRepo.findPageByDateRangePacked(
				new Pager().getPageRequest(1), DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-03-08"));
		Assert.assertNotEquals("No transaction record.", pg.getNumberOfElements(), 0);
		
		
		Iterator<OutSubPacked> itr = pg.getContent().iterator();

		Date startTime = new Date();
		while (itr.hasNext()) {

			OutSubPacked transaction = itr.next();
			
			log.info( "T. Number: "+transaction.getTransactionNumber());
			log.info( "Type: "+transaction.getType());
			log.info( "Status: "+transaction.getStatus());
			log.info( "Amount: "+transaction.getAmount());
			log.info( "Payer MSISDN: "+transaction.getPayer());
			log.info( "Payee MSISDN: "+transaction.getPayee() );
			
			log.info( "Payer Name: "+transaction.getPayerName() );
			log.info( "Payee Name: "+transaction.getPayeeName() );
			
			log.info( "Date: "+transaction.getDate());
			

		}
		
		long timeTaken = new Date().getTime() - startTime.getTime();
		log.info( "Time taken: "+( timeTaken/ ( 1000 ) )+"s(s)" );
	} 
	
	
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testNewSubPackedPageCount() throws ParseException { 
		
		Date startTime = new Date();
		Assert.assertNotNull("Fundamo repo is null.", subTRepo );
		Long count = subTRepo.findPageByDateRangePackedCount(DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-03-08"));
		Double amount = subTRepo.findByDateRangeAmount(DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-03-08"));

		log.info( "Parallel Count: "+count );
		log.info( "Parallel Amount: "+amount );
		
		long timeTaken = new Date().getTime() - startTime.getTime();
		log.info( "Time taken: "+( timeTaken/ ( 1000 ) )+"s(s)" );
	} 
	
	
	
	
	
	
	@Test
	// @Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testSubFindPageByDateRangePacked() throws ParseException { 

		Date startTime = new Date();
		Assert.assertNotNull("Fundamo repo is null.", tRepo);
		Slice<OutSubPacked> pg = tRepo.findPageByDateRangePackedSliced(
				new Pager().getPageRequest(1), DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-03-08"));
		Assert.assertNotEquals("No transaction record.", pg.getNumberOfElements(), 0);
		
		
		Iterator<OutSubPacked> itr = pg.getContent().iterator();

		while (itr.hasNext()) {

			OutSubPacked transaction = itr.next();
			
			log.info( "T. Number: "+transaction.getTransactionNumber());
			log.info( "Type: "+transaction.getType());
			log.info( "Status: "+transaction.getStatus());
			log.info( "Amount: "+transaction.getAmount());
			log.info( "Payer MSISDN: "+transaction.getPayer());
			log.info( "Payee MSISDN: "+transaction.getPayee() );
			
			log.info( "Payer Acc. No.: "+transaction.getPayerAccNo());
			log.info( "Payee Acc. No.: "+transaction.getPayeeAccNo() );
			
			log.info( "Date: "+transaction.getDate());
			

		}
		
		log.info( "Serial count: "+tRepo.findPageByDateRangePackedCount( DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-03-08"))
 );
		long timeTaken = new Date().getTime() - startTime.getTime();
		log.info( "Time taken: "+( timeTaken/ ( 1000 ) )+"s(s)" );
	} 
	
	
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testSubFindPageByTNoPacked() throws ParseException { 

		
		Assert.assertNotNull("Fundamo repo is null.", tRepo);
		Slice<OutSubPacked> pg = tRepo.findPageByTNoPacked( new Pager().getPageRequest(1), BigDecimal.valueOf( 3018L )
				, DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-03-08"));
		Assert.assertNotEquals("No transaction record.", pg.getNumberOfElements(), 0);
		
		
		Iterator<OutSubPacked> itr = pg.getContent().iterator();

		while (itr.hasNext()) {

			OutSubPacked transaction = itr.next();
			
			log.info( "T. Number: "+transaction.getTransactionNumber());
			log.info( "Type: "+transaction.getType());
			log.info( "Status: "+transaction.getStatus());
			log.info( "Amount: "+transaction.getAmount());
			log.info( "Payer MSISDN: "+transaction.getPayer());
			log.info( "Payee MSISDN: "+transaction.getPayee() );
			log.info( "Date: "+transaction.getDate());
			

		}
	} 
	
	
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testSubFindByTNoAmount() throws ParseException { 
		Assert.assertNotNull("Fundamo repo is null.", tRepo);
		double amount = tRepo.findByTNoAmount( BigDecimal.valueOf( 3018L )
				, DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-03-08"));
		log.info( "Amount: "+amount );	
	} 
	
	
	
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testSubFindPageByPayeeAccNoPacked() throws ParseException { 

		
		Assert.assertNotNull("Fundamo repo is null.", tRepo);
		Slice<OutSubPacked> pg = tRepo.findPageByPayeeAccNoPacked( new Pager().getPageRequest(1), "229"
				, DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-03-08") );
		Assert.assertNotEquals("No transaction record.", pg.getNumberOfElements(), 0);
		
		
		
		Iterator<OutSubPacked> itr = pg.getContent().iterator();

		while (itr.hasNext()) {

			OutSubPacked transaction = itr.next();
			
			log.info( "T. Number: "+transaction.getTransactionNumber());
			log.info( "Type: "+transaction.getType());
			log.info( "Status: "+transaction.getStatus());
			log.info( "Amount: "+transaction.getAmount());
			log.info( "Payer MSISDN: "+transaction.getPayer());
			log.info( "Payee MSISDN: "+transaction.getPayee() );
			log.info( "Date: "+transaction.getDate());
			

		}
	} 
	
	
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testSubFindByPayeeAccNoAmount() throws ParseException { 
		Assert.assertNotNull("Fundamo repo is null.", tRepo);
		double amount = tRepo. findByPayeeAccNoAmount( "229", DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-03-08") );
		log.info( "Amount: "+amount );	
	} 
	
	
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testSubFindPageByPayerAccNoPacked() throws ParseException { 

		
		Assert.assertNotNull("Fundamo repo is null.", tRepo);
		Slice<OutSubPacked> pg = tRepo.findPageByPayerAccNoPacked( new Pager().getPageRequest(1), "229"
				, DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-03-08") );
		Assert.assertNotEquals("No transaction record.", pg.getNumberOfElements(), 0);
		
		
		
		Iterator<OutSubPacked> itr = pg.getContent().iterator();

		while (itr.hasNext()) {

			OutSubPacked transaction = itr.next();
			
			log.info( "T. Number: "+transaction.getTransactionNumber());
			log.info( "Type: "+transaction.getType());
			log.info( "Status: "+transaction.getStatus());
			log.info( "Amount: "+transaction.getAmount());
			log.info( "Payer MSISDN: "+transaction.getPayer());
			log.info( "Payee MSISDN: "+transaction.getPayee() );
			log.info( "Date: "+transaction.getDate());
			

		}
	} 
	
	
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testSubFindByPayerAccNoAmount() throws ParseException { 
		Assert.assertNotNull("Fundamo repo is null.", tRepo);
		double amount = tRepo. findByPayerAccNoAmount( "229", DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-03-08") );
		log.info( "Amount: "+amount );	
	} 
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testSubFindByPageDateRange() throws ParseException {

		Date startTime = new Date();
		Assert.assertNotNull("Fundamo repo is null.", tRepo);
		Slice<OutSubPacked> pg = tRepo.findPageByDateRangePackedSliced(
				new Pager().getPageRequest(1, 15), DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2018-03-08"));
		Assert.assertNotEquals("No transaction record.", pg.getNumberOfElements(), 0);
		
		log.info( "No. of records: "+pg.getNumberOfElements() );
		
		
		
		
		
		Iterator<OutSubPacked> itr = pg.getContent().iterator();

		
		while (itr.hasNext()) {

			OutSubPacked transaction = itr.next();
			
			log.info( "T. Number: "+transaction.getTransactionNumber() );
			log.info( "Type: "+transaction.getType() );
			log.info( "Status: "+transaction.getStatus() );
			log.info( "Amount: "+transaction.getAmount() );
			log.info( "Payer MSISDN: "+transaction.getPayer() );
			log.info( "Payee MSISDN: "+transaction.getPayee() );
			log.info( "E. Type: "+transaction.geteType() );
			log.info( "Date: "+transaction.getDate() );
			

		}
		
		long timeTaken = new Date().getTime() - startTime.getTime();
		log.info( "Time taken: "+( timeTaken/ ( 1000 ) )+"s(s)" );
	} 
	
	
	
	/********************************************* Mechant transaction tests ******************************/
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testMerchantFindPageByDateRangeSliced() throws ParseException {

		Date startTime = new Date();
		Assert.assertNotNull( "Fundamo repo is null.", merRepo );
		Slice<Entry001> pg = merRepo.findPageByDateRangeSliced(
				new Pager().getPageRequest(1, 15), DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-02-08"));
		Assert.assertNotEquals("No transaction record.", pg.getNumberOfElements(), 0);
		log.info( "No. of records: "+pg.getNumberOfElements() );
		Iterator<Entry001> itr = pg.getContent().iterator();

		
		while (itr.hasNext()) {

			Entry001 entry = itr.next();
			Transaction001 transaction = entry.getTransaction001();
			UserAccount001 uAcc = entry.getUserAccount001();
			if( uAcc == null )
				uAcc = new UserAccount001();
			
			CorporateAccountHolder001 corp = uAcc.getCorporateAccountHolder001();
			if( corp == null )
				corp = new CorporateAccountHolder001();
			
			List< AccountIdentifier001 > payerAIList = transaction.getPayerAccountIdentifier001s();
			List< AccountIdentifier001 > payeeAIList = transaction.getPayeeAccountIdentifier001s();
			
			String entryTypeName = entry.getEntryType001().getSystemCode().getValue();
			String txnStatus = transaction.getSystemCode().getValue();
			
			
			
			
			log.info( "T. Number: "+transaction.getTransactionNumber() );
			log.info( "Corp Name: "+corp.getName() );
			log.info( "Payer AI list size: "+payerAIList.size());
			log.info( "Payee AI list size: "+payeeAIList.size());
			log.info( "Type: "+entryTypeName );
			
			
			
			// log.info( "Type: "+transaction.getType() );
			log.info( "Status: "+txnStatus );
			// log.info( "Amount: "+transaction.getAmount() );
			// log.info( "Payer MSISDN: "+transaction.getPayer() );
			// log.info( "Payee MSISDN: "+transaction.getPayee() );
			// log.info( "Date: "+transaction.getDate() );
			

		} 
		
		
		
		
		
		long timeTaken = new Date().getTime() - startTime.getTime();
		log.info( "Time taken: "+( timeTaken/ ( 1000 ) )+"s(s)" );
	
	} 
	
	
	
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testMerchantFindPageByDateRangePackedSliced() throws ParseException {

		Date startTime = new Date();
		Assert.assertNotNull( "Fundamo repo is null.", merRepo );
		Slice<OutMerPacked> pg = merRepo.findPageByDateRangePackedSliced(
				new Pager().getPageRequest(1, 15), DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-02-08"));
		Assert.assertNotEquals("No transaction record.", pg.getNumberOfElements(), 0);
		log.info( "No. of records: "+pg.getNumberOfElements() );
		Iterator<OutMerPacked> itr = pg.getContent().iterator();
		while (itr.hasNext()) {

			OutMerPacked record = itr.next();
			
			log.info( "Corp Name: "+record.getName() );
			log.info( "Payer MSISDN: "+record.getPayer());
			log.info( "Payee MSISDN: "+record.getPayee());
			
			log.info( "Payer Acc. No.: "+record.getPayerAccNo());
			log.info( "Payee Acc. No.: "+record.getPayeeAccNo() );
		} 
		
		long timeTaken = new Date().getTime() - startTime.getTime();
		log.info( "Time taken: "+( timeTaken/ ( 1000 ) )+"s(s)" );
	
	} 
	
	
	
	
	/********************************************* Sub. Reg. Test ***********************************/
	
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testSubRegFindPageByDateRangePackedSliced() throws ParseException {

		Date startTime = new Date();
		Assert.assertNotNull( "Fundamo repo is null.", merRepo );
		Slice< OutSubRegPacked > pg = subRegRepo.findPageByDateRangePackedSliced(
				new Pager().getPageRequest(1, 15), DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-02-08"));
		Assert.assertNotEquals("No transaction record.", pg.getNumberOfElements(), 0);
		
		log.info( "No. of records: "+pg.getNumberOfElements() );
		
		
		Iterator< OutSubRegPacked > itr = pg.getContent().iterator();

		
		while (itr.hasNext()) {

			OutSubRegPacked record = itr.next();
			
			
			log.info( "Name: "+record.getName() );
			log.info( "MSISDN: "+record.getMsisdn() );
			log.info( "ID Type: "+record.getIdType() );
			log.info( "ID NO: "+record.getIdNo() );
			log.info( "Dob: "+record.getDob() );
			log.info( "Status: "+record.getStatus() );
			log.info( "Reg. Date: "+record.getDate() );
			

		} 
		
		
		
		
		
		long timeTaken = new Date().getTime() - startTime.getTime();
		log.info( "Time taken: "+( timeTaken/ ( 1000 ) )+"s(s)" );
	
	} 
	
	
	
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testSubRegFindPageByDateRangeSliced() throws ParseException {

		Date startTime = new Date();
		Assert.assertNotNull( "Fundamo repo is null.", merRepo );
		Slice<Subscriber001> pg = subRegRepo.findPageByDateRangeSliced(
				new Pager().getPageRequest(1, 15), DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-02-08"));
		Assert.assertNotEquals("No transaction record.", pg.getNumberOfElements(), 0);
		
		log.info( "No. of records: "+pg.getNumberOfElements() );
		
		Iterator<Subscriber001> itr = pg.getContent().iterator();

		
		while (itr.hasNext()) {
			
			Subscriber001 sub = itr.next();
			//Transaction001 transaction = entry.getTransaction001();
			UserAccount001 uAcc = sub.getUserAccount001();
			if( uAcc == null )
				uAcc = new UserAccount001();
			
			
			
			Person001 person = sub.getPerson001();
			if( person == null )
				person = new Person001();  
			
			List< RegistrationRequestData001 > dataList = person.getRegistrationRequestData001s();
			
			log.info( "Reg. data list size: "+dataList.size() );
			log.info( "Name: "+person.getEmailAddress() );
			log.info( "Sub Name: "+sub.getName() ); 
		} 
		
		long timeTaken = new Date().getTime() - startTime.getTime();
		log.info( "Time taken: "+( timeTaken/ ( 1000 ) )+"s(s)" );
	
	} 
	
	
	
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testFindByPageDateRangeSubRegThroughUA() throws ParseException {

		Date startTime = new Date();
		Assert.assertNotNull( "Fundamo repo is null.", merRepo );
		Slice<UserAccount001> pg = uAccRepo.findPageByDateRangeSliced(
				new Pager().getPageRequest(1, 15), DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-02-08"));
		Assert.assertNotEquals("No transaction record.", pg.getNumberOfElements(), 0);
		
		log.info( "No. of records: "+pg.getNumberOfElements() );
		
		Iterator<UserAccount001> itr = pg.getContent().iterator();

		
		while (itr.hasNext()) {
			
			UserAccount001 uAcc = itr.next();
			Subscriber001 sub =  uAcc.getSubscriber001();
			//Transaction001 transaction = entry.getTransaction001();
			
			if( sub == null )
				sub = new Subscriber001(); 
			
			
			
			
			Person001 person = sub.getPerson001();
			if( person == null )
				person = new Person001(); 
			
			
			//List< Person001 > personList = sub.getPerson001s();
			//log.info( "Person data list size: "+personList.size() ); 
			
		
			/*
			Person001 person = null;
			if( personList.size() == 0 )
				new Person001();
			else 
				person = personList.get( 0 ); */
			
			List< RegistrationRequestData001 > dataList = person.getRegistrationRequestData001s();
			
			log.info( "Reg. data list size: "+dataList.size() );
			log.info( "Name: "+person.getEmailAddress() );
			log.info( "Sub Name: "+sub.getName() ); 
		} 
		
		
		long timeTaken = new Date().getTime() - startTime.getTime();
		log.info( "Time taken: "+( timeTaken/ ( 1000 ) )+"s(s)" );
	
	} 
	
	
	
	
	
	@Test
	@Ignore
	@Transactional( propagation = Propagation.REQUIRES_NEW, value = "fundamoTransactionManager" )
	public void testAccountIdentifierRepoMSISDN() throws ParseException {

		//AccountIdentifier001Repo aiRepo = springAppContext.getBean( AccountIdentifier001Repo.class );
		
		/*
		Assert.assertNotNull("Fundamo repo is null.", aiRepo );
		Slice<AccountIdentifier001> pg = aiRepo.findPageByDateRange(
				new Pager().getPageRequest(1), DateFormatFac.toDate("2010-02-01"), DateFormatFac.toDateUpperBound("2010-02-02"));
		Assert.assertNotEquals("No transaction record.", pg.getContent().size(), 0);
		
		log.info( "Rows: "+pg.getNumberOfElements() ); */
		
		
		
		//Iterator<AccountIdentifier001> itr = pg.getContent().iterator();
		Iterator<AccountIdentifier001> itr = new ArrayList<AccountIdentifier001>().iterator();

		while (itr.hasNext()) {

			// Entry001 entry = itr.next();
			AccountIdentifier001 ai = itr.next();
			List< Transaction001 > transactionList = ai.getTransaction001s();
			log.info( "Transaction count: "+transactionList.size() );
			// Transaction001 transaction = ai.getTransaction001();//entry.getTransaction001();
			//TransactionType001 type = transaction.getTransactionType001();

			
			//UserAccount001 uAcc = entry.getUserAccount001();
			
			
			// UserAccount001 payerUA = uAcc.getSubscriber001().getUserAccount001();
			// List< AccountIdentifier001 > payerAIList= transaction.getAccountIdentifier001s();
			// AccountIdentifier001 payerAIList= transaction.getAccountIdentifier001();
			// List< AccountIdentifier001 > payeeAIList= transaction.getPayeeUserAccount001().getAccountIdentifier001s();
			
			
			
			// Assert.assertNotNull( "Payee user account is null.", payeeAIList );
			// log.info( "Payee list size: "+payeeAIList.size() );
			
			// Assert.assertNotNull( "Payer user account is null.", payerAIList );
			// log.info( "Payer list size: "+payerAIList.size() );
			
		}
			
	
		//log.info("Results count: " + pg.getTotalElements());
	}
}
