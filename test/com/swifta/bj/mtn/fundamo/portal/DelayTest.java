package com.swifta.bj.mtn.fundamo.portal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;

import com.swifta.bj.mtn.fundamo.portal.controller.main.DelayObj;

public class DelayTest {

	private static Logger log = LogManager.getLogger();

	public class WakerThread extends Thread {
		private DelayObj delayObj;
		transient long timeout = 0;

		public WakerThread(DelayObj delayObj, long timeout) {
			this.delayObj = delayObj;
			this.timeout = timeout;

		}

		

		private synchronized void waiter() throws InterruptedException {
			this.wait(1000);
		}

		@Override
		public void run() {
			log.info("Waker thread started...");
			try {

				while (timeout > 0) {
					log.info("Waker thread loop started...");
					log.info("Pre sleep timeout value: " + timeout + " ms(s) ");
					timeout = timeout - 10;
					this.waiter();
					log.info("Waker thread relooping [ after 10 ms(s)...");
					log.info("Post sleep timeout value: " + timeout + " ms(s) ");
				}
				delayObj.setDelay(false);

			} catch (InterruptedException e) {
				e.printStackTrace();
				log.info("Timeout thread has been interrupted. Re-running it...");
				run();
			}
		}
	}

	@Test
	public synchronized void just() throws InterruptedException {

		// Test 3 - synchronized method of object
		DelayObj delayObj = new DelayObj();
		delayObj.setDelay(true);
		new WakerThread(delayObj, 100 ).start();
		log.info("Delay started...");
		delayObj.delay(delayObj);

		log.info("Delay complete.");

	}

}
