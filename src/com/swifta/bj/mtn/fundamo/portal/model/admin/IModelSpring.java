package com.swifta.bj.mtn.fundamo.portal.model.admin;

public interface IModelSpring< R > extends IModel< R > {

	public Long getUserAuthId();
	public void setUserAuthId(Long userAuthId);
	public String getUserAuthSession();
	public void setUserAuthSession(String userAuthSession);
	
}
