package com.swifta.bj.mtn.fundamo.portal.model.admin;

import java.util.Collection;
import java.util.Date;
import com.swifta.bj.mtn.fundamo.portal.bean.AbstractDataBean;
import com.swifta.bj.mtn.fundamo.portal.bean.In;
import com.swifta.bj.mtn.fundamo.portal.bean.InTxn;
import com.swifta.bj.mtn.fundamo.portal.bean.Out;
import com.swifta.bj.mtn.fundamo.portal.bean.OutTxnMeta;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;

public interface IModel< R > {
	Out search( In in, BeanItemContainer<AbstractDataBean> container );
	Out set(In in, BeanItemContainer<AbstractDataBean> container);
	Out setMeta(In in, OutTxnMeta outSubscriber);
	Out searchMeta(In in, OutTxnMeta outSubscriber);
	Out setExportData(In in, BeanItemContainer<AbstractDataBean> container);
	Out setExportDataMulti(In in,
			BeanItemContainer<AbstractDataBean> container, Collection<Item> records);
	Date getExportFDate( InTxn inTxn, R repo );
}
