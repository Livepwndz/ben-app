package com.swifta.bj.mtn.fundamo.portal.model.main;

import java.io.Serializable;
import java.math.BigInteger;
import java.security.SecureRandom;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.swifta.bj.mtn.fundamo.portal.bean.BData;
import com.swifta.bj.mtn.fundamo.portal.bean.In;
import com.swifta.bj.mtn.fundamo.portal.bean.InUserDetails;
import com.swifta.bj.mtn.fundamo.portal.bean.Out;
import com.vaadin.data.Item;

public class MUserSelfCare extends ModelSelfCare implements Serializable  {

	private static final long serialVersionUID = 1L;

	private Logger log = LogManager.getLogger(MUserSelfCare.class.getName());

	private InUserDetails inUser;
	private ApplicationContext springAppContext;

	public MUserSelfCare() {
		if (dataSource == null) {
			log.error("DataSource is null.");
			throw new IllegalStateException("DataSource cannot be null.");
		}

		log.debug(" Model initialized successfully.");

	}

	public MUserSelfCare(ApplicationContext springAppContext) {

		this.setSpringAppContext(springAppContext);

	}

	public ApplicationContext getSpringAppContext() {
		return springAppContext;
	}

	public void setSpringAppContext(ApplicationContext springAppContext) {
		this.springAppContext = springAppContext;
	}



	@SuppressWarnings("unchecked")
	private String generatePassHash(Item record) {
		record.getItemProperty("passSalt").setValue(sha256Hex(nextSessionId()));
		return sha256Hex(record.getItemProperty("password").getValue()
				.toString()
				+ record.getItemProperty("passSalt").getValue().toString());
	}

	private String nextSessionId() {
		SecureRandom random = new SecureRandom();
		return new BigInteger(130, random).toString(32);
	}

	@SuppressWarnings("unchecked")
	public Out resetUserCreds( String email, In in) {

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		

		String q = "UPDATE users AS u";
		q += " SET u.change_password = ?, u.user_session = ?, u.password = ?, u.pass_salt = ?";
		q += " WHERE u.email = ?";

		try {

			conn = dataSource.getConnection();
			conn.setReadOnly(false);

			BData<?> bInData = in.getData();
			inUser = (InUserDetails) bInData.getData();
			
			log.debug( "Reset password user email: "+email );

			// Authorization
			if ( checkEmail( email, inUser.getRecord()  )
					.getStatusCode() != 1) {
				log.debug("Login session: " + inUser.getUserSession());
				return out;
			}

			

			out = new Out();
			
			if ( email == null || email.trim().isEmpty() ) {
				log.debug("No user data - user id");
				out.setMsg("No user data.");
				out.setStatusCode( 100 );
				return out;
			}


			

			// String session = nextSessionId();
			String newPassword = inUser.getRecord()
					.getItemProperty("newPassword").getValue().toString();

			// This is necessary for generatePassHash to base on newPassword
			// other than current password
			inUser.getRecord().getItemProperty("password")
					.setValue(newPassword);

			ps = conn.prepareStatement(q);
			ps.setInt(1, 1);
			ps.setString(2, null );
			ps.setString(3, this.generatePassHash(inUser.getRecord()));
			ps.setString(4, inUser.getRecord().getItemProperty("passSalt")
					.getValue().toString());
			ps.setString(5, email);
			
			log.debug("Query: " + ps.toString());
			
			ps.executeUpdate();

			inUser.getRecord().getItemProperty("password").setValue(null);
			inUser.getRecord().getItemProperty("newPassword").setValue(null);
			inUser.getRecord().getItemProperty("passSalt").setValue(null);

			out.setMsg("User creds reset successfully.");
			out.setStatusCode(1);

		} catch (Exception e) {
			log.error(e.getMessage());
			out.setMsg("Could not complete operation. ");
			e.printStackTrace();

		} finally {
			connCleanUp(conn, ps, rs);
		}

		return out;
	}



	


	@SuppressWarnings("unchecked")
	public Out checkEmail( String email, Item record ) {

		Connection conn = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		String q = "SELECT u.user_id, u.username, u.last_login, u.user_session FROM users AS u";
		q += " WHERE u.email = ? ";
		q += " LIMIT 1;";

		try {

			// TODO Check if user session is valid before operation.
			// TODO Check if user profile is authorized
			// TODO This should be implemented in one place, the mother class
			conn = dataSource.getConnection();
			conn.setReadOnly(true);
			
			out = new Out();

			ps = conn.prepareStatement(q);
			ps.setString(1, email);

			log.debug("Query: " + ps.toString());

			rs = ps.executeQuery();
			if ( rs.next()) {
				
				record.getItemProperty("username").setValue( rs.getString( "username" ) );
				record.getItemProperty("lastLogin").setValue( rs.getTimestamp( "last_login" ) );
				
				
				String userSession = rs.getString( "user_session" );
				if( userSession == null || userSession.trim().isEmpty()){
					userSession = null;
					// log.info( "Reset login session is null." );
				}
				
				record.getItemProperty("resetLoginSession").setValue( userSession );
				
				
				log.debug("Email exists");
				out.setMsg("Email exists");
				out.setStatusCode(1);
				

				return out;
			}

			out.setMsg("Email is not known to the system.");

		} catch (Exception e) {
			log.error(e.getMessage());
			out.setMsg("Could not complete operation. ");
			e.printStackTrace();

		} finally {
			connCleanUp(conn, ps, rs);
		}

		return out;
	}




}
