package com.swifta.bj.mtn.fundamo.portal.model.admin;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;

import com.swifta.bj.mtn.fundamo.portal.bean.AbstractDataBean;
import com.swifta.bj.mtn.fundamo.portal.bean.BData;
import com.swifta.bj.mtn.fundamo.portal.bean.ExportMerchant;
import com.swifta.bj.mtn.fundamo.portal.bean.In;
import com.swifta.bj.mtn.fundamo.portal.bean.InTxn;
import com.swifta.bj.mtn.fundamo.portal.bean.Out;
import com.swifta.bj.mtn.fundamo.portal.bean.OutMerPacked;
import com.swifta.bj.mtn.fundamo.portal.bean.OutSubscriber;
import com.swifta.bj.mtn.fundamo.portal.bean.OutSubscriberTest;
import com.swifta.bj.mtn.fundamo.portal.bean.OutTxnMeta;
import com.swifta.bj.mtn.fundamo.portal.model.util.DateFormatFac;
import com.swifta.bj.mtn.fundamo.portal.model.util.DateFormatFacRuntime;
import com.swifta.bj.mtn.fundamo.portal.model.util.NumberFormatFac;
import com.swifta.bj.mtn.fundamo.portal.model.util.Pager;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo.Entry001Repo;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo.Transaction001Repo;
import com.vaadin.data.Item;
import com.vaadin.data.Property;
import com.vaadin.data.util.BeanItemContainer;


public class MMerchant extends MSpringDAO implements IModelSpring<Entry001Repo>,
		Serializable {

	private static final long serialVersionUID = 1L;
	
	private Date fDate;
	private long rowCount = 0L;
	private double tAmount = 0D;
	private Property< String > totalRecord;
	private Property< String > totalRevenue;
	

	private Logger log = LogManager.getLogger(MMerchant.class.getName());

	public MMerchant( ApplicationContext cxt) {
		super(cxt);
		log.debug(" MDAO initialized successfully.");
	}

	public Date getfDate() {
		return fDate;
	}
	
	public void setfDate(Date fDate) {
		this.fDate = fDate;
	}
	
	public long getRowCount() {
		return rowCount;
	}
	
	public synchronized void setRowCount(Long rowCount) {
	
		totalRecord.setValue( NumberFormatFac.toThousands( rowCount + "" ) );
		this.rowCount = rowCount;
	}
	public double gettAmount() {
		return tAmount;
	}
	public synchronized void settAmount(double tAmount) {
		totalRevenue.setValue(NumberFormatFac.toMoney( ( tAmount / 100 ) +"" ) );
		this.tAmount = tAmount;
	}



	

	@Override
	// @Transactional( propagation = Propagation.REQUIRED, value = "fundamoTransactionManager" )
	public Out set(In in, BeanItemContainer<AbstractDataBean> container) {

		Out out = this.checkAuthorization();
		if (out.getStatusCode() != 1) {
			out.setStatusCode(100);
			return out;
		}
		out = new Out();

		// TODO Check if user session is valid before operation.
		// TODO Check if user profile is authorized
		// TODO This should be implemented in one place, the mother class

		// Set relevant data;
		// From com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity to UI component datasource convenience bean.

		OutSubscriber outSubscriber = new OutSubscriber();
		container.addBean(outSubscriber);

		out.setStatusCode(1);
		out.setMsg("Data fetch successful.");
		return out;
	}

	@Override
	// @Transactional( propagation = Propagation.REQUIRED, value = "fundamoTransactionManager" )
	public Out search(In in, BeanItemContainer<AbstractDataBean> container) {

		Out out = this.checkAuthorization();
		if (out.getStatusCode() != 1) {
			out.setStatusCode(100);
			return out;
		} else {
			
		}
		
		out = new Out();

		try {

			BData<?> bInData = in.getData();
			InTxn inTxn = (InTxn) bInData.getData();
			boolean isPgNav = inTxn.isPgNav();
			inTxn.setPgNav(false);

			/*
			// [ Initialize page & revenue on any db call??? ] Noooo... only on
			// some calls.
			if (!inTxn.isExportOp()) {
				if (!isPgNav) {
					OutTxnMeta meta = inTxn.getMeta();
					meta.getTotalRecord().setValue("0");
					meta.getTotalRevenue().setValue("0.00");
				}
			}*/
			
			rowCount = 0L;
			tAmount = 0D;

			// [ Initialize page & revenue on any db call??? ] Noooo... only on
			// some calls.
			if (!inTxn.isExportOp()) {
				if (!isPgNav) {
					OutTxnMeta meta = inTxn.getMeta();
					totalRecord = meta.getTotalRecord();
					totalRevenue = meta.getTotalRevenue();
					meta.getTotalRecord().setValue("0");
					meta.getTotalRevenue().setValue("0.00");
				}
			}

			Entry001Repo repo = springAppContext
					.getBean(Entry001Repo.class);
			if (repo == null) {
				log.debug("Transaction001 repo is null");
				out.setMsg("DAO error occured.");
				return out;
			}

			Slice< OutMerPacked > slice = null;
			Iterator< OutMerPacked > itr = null;

			Pager pager = springAppContext.getBean(Pager.class);
			Map<String, Object> searchMap = inTxn.getSearchMap();
			Set<String> searchKeySet = searchMap.keySet();

			log.debug("MMer from date:" + inTxn.getfDate(), this);
			log.debug("MMer to date:" + inTxn.gettDate(), this);

			Pageable pgR = null;
			BeanItemContainer<OutMerPacked> exportRawData = null;
			// double tAmount = 0D;
			// long rowCount = 0L;

			// Date fall back

			if (inTxn.getfDate() == null || inTxn.gettDate() == null) {
				inTxn.setfDate("2010-02-01");
				inTxn.settDate("2010-02-03");
			}

			fDate = DateFormatFacRuntime.toDate(inTxn.getfDate());

			if (inTxn.isExportOp()) {
				fDate = this.getExportFDate(inTxn, repo);
				pgR = pager.getPageRequest(0, inTxn.getExportPgLen());
				exportRawData = new BeanItemContainer<>(OutMerPacked.class);
			} else {
				pgR = pager.getPageRequest(inTxn.getPage());
			}

			boolean isSearch = false;

			if (searchKeySet.size() != 0) {
				if (searchKeySet.contains("column3")) {

					Object val = searchMap.get("column3");
					if (val != null && !val.toString().trim().isEmpty()) {
						isSearch = true;
						BigDecimal tNo = BigDecimal.valueOf(Long
								.valueOf((String) val));
						
						boolean isSlicer = true;
						if( !inTxn.isExportOp() ){
							if( !isPgNav ){
								Page< OutMerPacked >  packedPg = repo.findPageByTNoPacked(pgR, tNo, fDate, DateFormatFac
										.toDateUpperBound(inTxn
												.gettDate()) );
								
								
								// TODO Load amount on new thread.
								new Thread( new Runnable() {
									
									@Override
									public void run() {
										
										log.info( "Amount thread running..." );
										tAmount = repo
												.findByTNoAmount( tNo, fDate, DateFormatFacRuntime.toDateUpperBound(inTxn.gettDate()) );
										settAmount( tAmount );
										log.info( "Amount thread complete!" );
									}
								}).start();
							
								rowCount =  packedPg.getTotalElements();
								this.setRowCount(rowCount);
								slice = packedPg;
								isSlicer = false;
							}
							
						} 
						
						if ( isSlicer ){
							slice = repo.findPageByTNoPackedSliced(pgR, tNo, fDate,
									DateFormatFac.toDateUpperBound(inTxn.gettDate()));
						}
						
						
						if (!inTxn.isExportOp()) {
							if (!isPgNav)
								;/*tAmount = repo
										.findByTNoAmount( tNo, fDate, DateFormatFac.toDateUpperBound(inTxn.gettDate()) );*/
						}



					}

				} else if (searchKeySet.contains("column9")) {

					Object val = searchMap.get("column9");

					if (val != null && !val.toString().trim().isEmpty()) {
						isSearch = true;
						
						
						boolean isSlicer = true;
						if( !inTxn.isExportOp() ){
							if( !isPgNav ){
								Page< OutMerPacked >  packedPg = repo.findPageByPayerAccNoPacked(pgR, ( String ) val, fDate, DateFormatFac
										.toDateUpperBound(inTxn
												.gettDate()) );
								
								// TODO Load amount on new thread.
								new Thread( new Runnable() {
									
									@Override
									public void run() {
										
										log.info( "Amount thread running..." );
										tAmount = repo
												.findByPayerAccNoAmount(
														(String) val, fDate,
														DateFormatFacRuntime
																.toDateUpperBound(inTxn
																		.gettDate()));										settAmount( tAmount );
										log.info( "Amount thread complete!" );
									}
								}).start();
							
								rowCount =  packedPg.getTotalElements();
								this.setRowCount(rowCount);
								slice = packedPg;
								isSlicer = false;
							}
							
						} 
						
						if ( isSlicer ){
							slice = repo.findPageByPayerAccNoPackedSliced(pgR, ( String ) val, fDate,
									DateFormatFac.toDateUpperBound(inTxn.gettDate()));
						}
						
						
						
						if (!inTxn.isExportOp()) {
							if (!isPgNav)
								;/*tAmount = repo
										.findByPayerAccNoAmount(
												(String) val, fDate,
												DateFormatFac
														.toDateUpperBound(inTxn
																.gettDate()));*/
						}
					}

				} else if (searchKeySet.contains("column10")) {

					Object val = searchMap.get("column10");
					if (val != null && !val.toString().trim().isEmpty()) {
						isSearch = true;
						
						
						boolean isSlicer = true;
						if( !inTxn.isExportOp() ){
							if( !isPgNav ){
								Page< OutMerPacked >  packedPg = repo.findPageByPayeeAccNoPacked(pgR, ( String ) val, fDate, DateFormatFac
										.toDateUpperBound(inTxn
												.gettDate()) );
								
								// TODO Load amount on new thread.
								new Thread( new Runnable() {
									
									@Override
									public void run() {
										
										log.info( "Amount thread running..." );
										tAmount = repo
												.findByPayeeAccNoAmount(
														(String) val, fDate,
														DateFormatFacRuntime
																.toDateUpperBound(inTxn
																		.gettDate()));										settAmount( tAmount );
										log.info( "Amount thread complete!" );
									}
								}).start();
							
								rowCount =  packedPg.getTotalElements();
								this.setRowCount(rowCount);
								slice = packedPg;
								isSlicer = false;
							}
							
						} 
						
						if ( isSlicer ){
							slice = repo.findPageByPayeeAccNoPackedSliced(pgR, ( String ) val, fDate,
									DateFormatFac.toDateUpperBound(inTxn.gettDate()));
						}
						
						
						if (!inTxn.isExportOp()) {
							if (!isPgNav)
								;/*tAmount = repo
										.findByPayeeAccNoAmount(
												(String) val, fDate,
												DateFormatFac
														.toDateUpperBound(inTxn
																.gettDate()));*/
						}
					}

				}

			} 

			if (!isSearch) {
				if (inTxn.getfDate() != null && inTxn.gettDate() != null) {

					
					boolean isSlicer = true;
					
					if( !inTxn.isExportOp() ){
						if( !isPgNav ){
							Page< OutMerPacked >  packedPg = repo.findPageByDateRangePacked(pgR, fDate,
									DateFormatFac.toDateUpperBound(inTxn.gettDate()));
							
							// TODO Load amount on new thread.
							new Thread( new Runnable() {
								
								@Override
								public void run() {
									
									log.info( "Amount thread running..." );
									tAmount = repo.findByDateRangeAmount(fDate,
											DateFormatFacRuntime.toDateUpperBound(inTxn
													.gettDate()));										settAmount( tAmount );
									log.info( "Amount thread complete!" );
								}
							}).start();
						
							rowCount =  packedPg.getTotalElements();
							this.setRowCount(rowCount);
							slice = packedPg;
							isSlicer = false;
						}
						
					} 
					
					if ( isSlicer ){
						slice = repo.findPageByDateRangePackedSliced(pgR, fDate,
								DateFormatFac.toDateUpperBound(inTxn.gettDate()));
					}

					// Amount should not be called in data export
					if ( !inTxn.isExportOp() ) {
						if (!isPgNav) {
							;/*tAmount = repo.findByDateRangeAmount(fDate,
									DateFormatFac.toDateUpperBound(inTxn
											.gettDate()));*/
						}
					}
				}
			}

			if ( slice == null) {
				log.info("Page object is null.");
				out.setMsg("DAO error occured.");
				return out;
			}

			
			if ( slice.getNumberOfElements() == 0 ) {

				log.info("Record count is 0.");
				container.addBean(new OutSubscriberTest());
				BData<BeanItemContainer<AbstractDataBean>> bOutData = new BData<>();
				bOutData.setData(container);
				out.setData(bOutData);
				out.setMsg("No records found.");

				return out;
			} 

			itr = slice.getContent().iterator();
			do {
				OutMerPacked record = itr.next();
				// TODO Remove comma separators from mony if it's export.
				if (inTxn.isExportOp())
					record.setAmount( record.getsAmount() );

				container.addBean(record);
				if (inTxn.isExportOp())
					exportRawData.addBean(record);

			} while (itr.hasNext()); 

			
			
			
			
			
			
			if (inTxn.isExportOp()) {
				BData<BeanItemContainer<OutMerPacked>> bData = new BData<>();
				bData.setData(exportRawData);
				out.setData(bData);
			} else {

				if (!isPgNav) {
					OutTxnMeta meta = inTxn.getMeta();
					// meta.getTotalRecord().setValue( rowCount + "");
					meta.getTotalRevenue().setValue("Loading...");
				}
			}

			out.setStatusCode(1);
			out.setMsg("Data fetch successful.");

		} catch (Exception e) {

			container.addBean(new OutSubscriber());
			BData<BeanItemContainer<AbstractDataBean>> bOutData = new BData<>();
			bOutData.setData(container);
			out.setData(bOutData);

			e.printStackTrace();
			out.setMsg("Data fetch error.");
		}

		return out;
	}

	@Override
	// @Transactional( propagation = Propagation.REQUIRED, value = "fundamoTransactionManager" )
	public Date getExportFDate(InTxn inTxn, Entry001Repo repo) {

		int fromPgNo = inTxn.getExportFPgNo();
		log.info("In export F-PgNo " + fromPgNo);

		int excludePgNo = fromPgNo - 1;
		if (fromPgNo <= 1) {
			excludePgNo = 1;
			fromPgNo = 1;
		}

		// - find max date in excludePgNo page of that.
		Slice<OutMerPacked> expoExcludePage = repo.findPageByDateRangePackedSliced(
				new Pager().getPageRequest(excludePgNo),
				DateFormatFacRuntime.toDate(inTxn.getfDate()),
				DateFormatFacRuntime.toDateUpperBound(inTxn.gettDate()));
		Date expoFDate = null;
		int tElements = expoExcludePage.getNumberOfElements();

		// - Get fast date of 1st date if fromPgNo == 1, else, get last date of
		// current page
		if (fromPgNo == 1)
			expoFDate = DateFormatFacRuntime.toDate (expoExcludePage.getContent().get(0).getDate() );
		else
			expoFDate = DateFormatFacRuntime.toDate ( expoExcludePage.getContent().get(tElements - 1)
					.getDate() );
		// - Probable latest date in exclude page [ still under testing ]
		log.info("Export F-Date?: " + expoFDate.toString());
		return expoFDate;
	}

	@SuppressWarnings("unchecked")
	@Override
	// @Transactional( propagation = Propagation.REQUIRED, value = "fundamoTransactionManager" )
	public Out setExportData(In in,
			BeanItemContainer<AbstractDataBean> container) {

		Out out = this.checkAuthorization();
		if (out.getStatusCode() != 1) {
			out.setStatusCode(100);
			return out;
		}
		out = new Out();

		try {

			BData<?> bInData = in.getData();
			InTxn inTxn = (InTxn) bInData.getData();

			log.debug("Page no: " + inTxn.getPage());
			log.debug("Page export limit: " + inTxn.getPageExportLimit());
			int exportPgLen = (int) Math.ceil(inTxn.getPageSize()
					* inTxn.getPageExportLimit());

			log.info("Export pg len: " + exportPgLen);
			log.info("Export start page: " + inTxn.getPage());

			inTxn.setExportPgLen(exportPgLen);
			inTxn.setExportOp(true);

			out = this.search(in, container);
			inTxn.setExportOp(false);

			log.debug("Feeder function returned. ");
			if (out.getStatusCode() != 1)
				return out;

			log.debug("Proceeding to package for export. ");
			// TODO Repackage data for export

			ModelMapper packer = springAppContext.getBean(ModelMapper.class);

			BeanItemContainer<OutMerPacked> rawData = (BeanItemContainer<OutMerPacked>) out
					.getData().getData();
			Iterator<OutMerPacked> itrRaw = rawData.getItemIds().iterator();
			BeanItemContainer<ExportMerchant> c = new BeanItemContainer<>(
					ExportMerchant.class);
			while (itrRaw.hasNext()) {
				OutMerPacked tRaw = itrRaw.next();
				ExportMerchant t = packer.map(tRaw, ExportMerchant.class);
				// Swap payer for description
				// String payee = tRaw.getColumn10();
				// t.setColumn8(payee);
				
				c.addBean(t);
			}

			BData<BeanItemContainer<ExportMerchant>> bData = new BData<>();
			bData.setData(c);
			out.setData(bData);
			out.setStatusCode(1);
			out.setMsg("Data fetch successful.");

		} catch (Exception e) {

			container.addBean(new OutSubscriber());
			BData<BeanItemContainer<AbstractDataBean>> bOutData = new BData<>();
			bOutData.setData(container);
			out.setData(bOutData);

			e.printStackTrace();
			out.setMsg("Data fetch error.");
		}

		return out;
	}

	@Override
	// @Transactional( propagation = Propagation.REQUIRED, value = "fundamoTransactionManager" )
	public Out setMeta(In in, OutTxnMeta meta) {
		// TODO Double check with original setTxnMeta, check for any variance
		// TODO I did check, can't tell the difference at the moment.
		return this.searchMeta(in, meta);
	}

	@Override
	// @Transactional( propagation = Propagation.REQUIRED, value = "fundamoTransactionManager" )
	public Out searchMeta(In in, OutTxnMeta meta) {

		Out out = this.checkAuthorization();
		if (out.getStatusCode() != 1) {
			out.setStatusCode(100);
			return out;
		}
		out = new Out();

		try {
			Transaction001Repo repo = springAppContext
					.getBean(Transaction001Repo.class);
			if (repo == null) {
				log.debug("Transaction001 repo is null");
				out.setMsg("DAO error occured - 1.");
				return out;
			}

			BData<?> bInData = in.getData();
			InTxn inTxn = (InTxn) bInData.getData();

			long rowCount = 0L;
			double amount = 0D;

			log.debug("MMer from date:" + inTxn.getfDate(), this);
			log.debug("MMer to date:" + inTxn.gettDate(), this);

			// TODO M - refactor
			/*
			if (inTxn.getfDate() == null || inTxn.gettDate() == null) {

				List<Object[]> lsObj = repo.getTotalAmountAndCountAll();
				if (lsObj != null) {
					Object[] obj = lsObj.get(0);
					rowCount = Long.valueOf(obj[1].toString());
					amount = Double.valueOf(obj[0].toString());
				}

			} else if (inTxn.getfDate() != null && inTxn.gettDate() != null) {
				log.debug("In date filter: ", this);

				List<Object[]> lsObj = repo.findByDateRangeAmountAndCount(
						DateFormatFac.toDate(inTxn.getfDate()),
						DateFormatFac.toDateUpperBound(inTxn.gettDate()));
				if (lsObj != null) {
					Object[] obj = lsObj.get(0);
					rowCount = Long.valueOf(obj[1].toString());
					amount = Double.valueOf(obj[0].toString());
				}
			} */

			log.info("Amount: " + amount);
			log.info("Total: " + rowCount);

			meta.getTotalRecord().setValue(rowCount + "");
			meta.getTotalRevenue().setValue((amount / 100) + "");

			out.setStatusCode(1);
			out.setMsg("Txn meta computed successfully.");

		} catch (Exception e) {
			e.printStackTrace();
			out.setMsg("Data fetch error.");
		}

		return out;
	}

	@Override
	public Out setExportDataMulti(In in,
			BeanItemContainer<AbstractDataBean> container,
			Collection<Item> records) {
		// TODO Auto-generated method stub
		return null;
	}

}
