package com.swifta.bj.mtn.fundamo.portal.model.util;

public class ParseRuntimeException extends RuntimeException{
	private static final long serialVersionUID = 1L;

	public ParseRuntimeException(String s) {
		super(s);
	}

}
