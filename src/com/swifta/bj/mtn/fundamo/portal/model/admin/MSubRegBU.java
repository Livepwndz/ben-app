package com.swifta.bj.mtn.fundamo.portal.model.admin;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.swifta.bj.mtn.fundamo.portal.bean.AbstractDataBean;
import com.swifta.bj.mtn.fundamo.portal.bean.BData;
import com.swifta.bj.mtn.fundamo.portal.bean.ExportSubReg;
import com.swifta.bj.mtn.fundamo.portal.bean.In;
import com.swifta.bj.mtn.fundamo.portal.bean.InTxn;
import com.swifta.bj.mtn.fundamo.portal.bean.Out;
import com.swifta.bj.mtn.fundamo.portal.bean.OutMerchant;
import com.swifta.bj.mtn.fundamo.portal.bean.OutSubReg;
import com.swifta.bj.mtn.fundamo.portal.bean.OutSubscriber;
import com.swifta.bj.mtn.fundamo.portal.bean.OutTxnMeta;
import com.swifta.bj.mtn.fundamo.portal.model.util.DateFormatFac;
import com.swifta.bj.mtn.fundamo.portal.model.util.DateFormatFacRuntime;
import com.swifta.bj.mtn.fundamo.portal.model.util.NumberFormatFac;
import com.swifta.bj.mtn.fundamo.portal.model.util.Pager;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.Person001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.RegistrationRequestData001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.Subscriber001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.Transaction001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.UserAccount001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo.Subscriber001Repo;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo.Transaction001Repo;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo.UserAccount001Repo;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;

public class MSubRegBU extends MDAO implements IModel<Subscriber001Repo>,
		Serializable {

	private static final long serialVersionUID = 1L;

	private Logger log = LogManager.getLogger(MSubRegBU.class.getName());

	public MSubRegBU(Long d, String s, String t, ApplicationContext cxt) {
		super(d, s, cxt);
		this.timeCorrection = " " + t;
		log.debug(" MDAO initialized successfully.");
	}

	@Override
	public Out set(In in, BeanItemContainer<AbstractDataBean> container) {

		Out out = this.checkAuthorization();
		if (out.getStatusCode() != 1) {
			out.setStatusCode(100);
			return out;
		}
		out = new Out();

		// TODO Check if user session is valid before operation.
		// TODO Check if user profile is authorized
		// TODO This should be implemented in one place, the mother class

		// Set relevant data;
		// From com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity to UI component datasource convenience bean.

		OutSubscriber outSubscriber = new OutSubscriber();
		container.addBean(outSubscriber);

		out.setStatusCode(1);
		out.setMsg("Data fetch successful.");
		return out;
	}

	@Override
	public Out search(In in, BeanItemContainer<AbstractDataBean> container) {

		Out out = this.checkAuthorization();
		if (out.getStatusCode() != 1) {
			out.setStatusCode(100);
			return out;
		}
		out = new Out();

		try {

			BData<?> bInData = in.getData();
			InTxn inTxn = (InTxn) bInData.getData();

			boolean isPgNav = inTxn.isPgNav();
			inTxn.setPgNav(false);

			// Initialize page & revenue on any db call.
			if (!inTxn.isExportOp()) {

				if (!isPgNav) {
					OutTxnMeta meta = inTxn.getMeta();
					meta.getTotalRecord().setValue("0");
					meta.getTotalRevenue().setValue("0,00");
				}
			}

			Subscriber001Repo repo = springAppContext
					.getBean(Subscriber001Repo.class);
			if (repo == null) {
				log.debug("Transaction001 repo is null");
				out.setMsg("DAO error occured.");
				return out;
			}

			Page<Subscriber001> pages = null;
			Pager pager = springAppContext.getBean(Pager.class);

			BeanItemContainer<OutSubReg> exportRawData = null;

			Map<String, Object> searchMap = inTxn.getSearchMap();
			Set<String> searchKeySet = searchMap.keySet();

			Pageable pgR = null;
			double tAmount = 0D;
			long rowCount = 0L;

			if (inTxn.getfDate() == null || inTxn.gettDate() == null) {
				inTxn.setfDate("2010-02-01");
				inTxn.settDate("2010-02-03");
			}

			Date fDate = DateFormatFacRuntime.toDate(inTxn.getfDate());

			if (inTxn.isExportOp()) {
				pgR = pager.getPageRequest(inTxn.getPage(),
						inTxn.getExportPgLen());
				fDate = this.getExportFDate(inTxn, repo);
				exportRawData = new BeanItemContainer<>(OutSubReg.class);
			} else {
				pgR = pager.getPageRequest(inTxn.getPage());
			}

			boolean isSearch = false;

			if (searchKeySet.size() != 0) {
				if (searchKeySet.contains("column1")) {

					Object val = searchMap.get("column1");
					if (val != null && !val.toString().trim().isEmpty()) {
						isSearch = true;
						pages = repo
								.findPageByName(pgR, (String) val, fDate,
										DateFormatFac.toDateUpperBound(inTxn
												.gettDate()));
					}

				}

			}

			if (!isSearch) {
				if (inTxn.getfDate() != null && inTxn.gettDate() != null) {
					log.debug("In date filter: ", this);
					
					pages = null;
					/*
					pages = repo.findPageByDateRange(pgR, fDate,
							DateFormatFac.toDateUpperBound(inTxn.gettDate()));*/
				}
			}

			if (pages == null) {
				log.debug("Page object is null.");
				out.setMsg("DAO error occured.");
				return out;
			}

			if (pages.getNumberOfElements() == 0) {

				container.addBean(new OutMerchant());
				BData<BeanItemContainer<AbstractDataBean>> bOutData = new BData<>();
				bOutData.setData(container);
				out.setData(bOutData);
				out.setMsg("No records found.");

				return out;
			}

			Iterator<Subscriber001> itr = pages.getContent().iterator();
			rowCount = pages.getTotalElements();
			log.debug("row count: " + rowCount, this);

			UserAccount001 ua = null;
			Subscriber001 sub = null;
			Person001 per = null;
			RegistrationRequestData001 regData = null;

			OutSubReg outSubReg = null;
			do {
				sub = itr.next();

				outSubReg = new OutSubReg();

				/*
				List<UserAccount001> uaList = sub.getUserAccount001s();
				if (uaList == null || uaList.size() == 0)
					ua = new UserAccount001();
				else
					ua = uaList.get(0); */
				
				ua = sub.getUserAccount001();
				if (ua == null)
					ua = new UserAccount001();
				

				per = sub.getPerson001();
				if (per == null)
					per = new Person001();

				List<RegistrationRequestData001> regDataList = per
						.getRegistrationRequestData001s();
				if (regDataList == null || regDataList.size() == 0)
					regData = new RegistrationRequestData001();
				else
					regData = regDataList.get(0);

				outSubReg.setName(sub.getName());
				outSubReg.setMsisdn(regData.getMsisdn());
				outSubReg.setIdNo(per.getIdNumber());
				outSubReg.setIdType(regData.getIdType());
				outSubReg.setDob(DateFormatFac.toStringDateOnly(per
						.getDateOfBirth()));
				outSubReg.setStatus(ua.getSystemCode().getCode());
				outSubReg
						.setRegDate(DateFormatFac.toString(sub.getLastUpdate()));

				container.addBean(outSubReg);
				if (inTxn.isExportOp())
					exportRawData.addBean(outSubReg);

			} while (itr.hasNext());

			if (inTxn.isExportOp()) {
				BData<BeanItemContainer<OutSubReg>> bData = new BData<>();
				bData.setData(exportRawData);
				out.setData(bData);
			} else {

				if (!isPgNav) {
					OutTxnMeta meta = inTxn.getMeta();
					meta.getTotalRecord().setValue(rowCount + "");
					meta.getTotalRevenue().setValue((tAmount / 100) + "");
				}
			}

			out.setStatusCode(1);
			out.setMsg("Data fetch successful.");

		} catch (Exception e) {

			container.addBean(new OutSubReg());
			BData<BeanItemContainer<AbstractDataBean>> bOutData = new BData<>();
			bOutData.setData(container);
			out.setData(bOutData);

			e.printStackTrace();
			out.setMsg("Data fetch error");
			log.error(e.getMessage(), this);
		}

		return out;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Out setExportData(In in,
			BeanItemContainer<AbstractDataBean> container) {

		Out out = this.checkAuthorization();
		if (out.getStatusCode() != 1) {
			out.setStatusCode(100);
			return out;
		}
		out = new Out();

		try {
			BData<?> bInData = in.getData();
			InTxn inTxn = (InTxn) bInData.getData();

			log.debug("Page no: " + inTxn.getPage());
			log.debug("Page export limit: " + inTxn.getPageExportLimit());
			int exportPgLen = (int) Math.ceil(inTxn.getPageSize()
					* inTxn.getPageExportLimit());

			inTxn.setExportPgLen(exportPgLen);
			inTxn.setExportOp(true);

			out = this.search(in, container);
			inTxn.setExportOp(false);

			if (out.getStatusCode() != 1)
				return out;

			// TODO Repackage data for export

			ModelMapper packer = springAppContext.getBean(ModelMapper.class);

			BeanItemContainer<OutSubReg> rawData = (BeanItemContainer<OutSubReg>) out
					.getData().getData();
			Iterator<OutSubReg> itrRaw = rawData.getItemIds().iterator();
			BeanItemContainer<ExportSubReg> c = new BeanItemContainer<>(
					ExportSubReg.class);
			while (itrRaw.hasNext()) {
				OutSubReg tRaw = itrRaw.next();
				ExportSubReg t = packer.map(tRaw, ExportSubReg.class);
				c.addBean(t);
			}

			BData<BeanItemContainer<ExportSubReg>> bData = new BData<>();
			bData.setData(c);
			out.setData(bData);

			out.setStatusCode(1);
			out.setMsg("Data fetch successful.");

		} catch (Exception e) {

			container.addBean(new OutSubscriber());
			BData<BeanItemContainer<AbstractDataBean>> bOutData = new BData<>();
			bOutData.setData(container);
			out.setData(bOutData);

			e.printStackTrace();
			out.setMsg("Data fetch error - 1");
		}

		return out;
	}

	@Override
	public Out setMeta(In in, OutTxnMeta outSubscriber) {

		Out out = this.checkAuthorization();
		if (out.getStatusCode() != 1) {
			out.setStatusCode(100);
			return out;
		}
		out = new Out();

		/*
		 * Perform the following ops. 1. Set total records 2. TODO Set Total
		 * revenue 3. TODO Filter by date:
		 */

		Transaction001Repo repo = (Transaction001Repo) springAppContext
				.getBean(Transaction001Repo.class);
		Page<Transaction001> pages = repo.findAll(new PageRequest(0, 1));
		outSubscriber.getTotalRecord().setValue(pages.getTotalPages() + "");

		out.setStatusCode(1);
		out.setMsg("Txn meta computed successfully.");

		return out;
	}

	@Override
	public Out searchMeta(In in, OutTxnMeta outSubscriber) {

		Out out = this.checkAuthorization();
		if (out.getStatusCode() != 1) {
			out.setStatusCode(100);
			return out;
		}
		out = new Out();

		BData<?> bInData = in.getData();
		InTxn inTxn = (InTxn) bInData.getData();

		/*
		 * Perform the following ops. 1. Set total records 2. TODO Set Total
		 * revenue 3. TODO Filter by date:
		 */

		Transaction001Repo repo = (Transaction001Repo) springAppContext
				.getBean(Transaction001Repo.class);
		Page<Transaction001> pages = repo.findAll(new PageRequest(0, 1));
		outSubscriber.getTotalRecord().setValue(pages.getTotalPages() + "");
		
		// TODO M - refactor
		/*
		outSubscriber.getTotalRevenue().setValue(
				(repo.getTotalPayeeAmount() / 100) + "");*/

		out.setStatusCode(1);
		out.setMsg("Txn meta computed successfully.");

		return out;
	}

	@Override
	public Out setExportDataMulti(In in,
			BeanItemContainer<AbstractDataBean> container,
			Collection<Item> records) {

		Out out = this.checkAuthorization();
		if (out.getStatusCode() != 1) {
			out.setStatusCode(100);
			return out;
		}

		out = new Out();

		// TODO Handle No record edge case
		/*
		 * rs = ps.executeQuery(); if( !rs.next() ) { log.debug( "No result" );
		 * out.setMsg( "No search result found." );
		 * 
		 * container.addBean( outTxn );
		 * 
		 * BData<BeanItemContainer<OutTxn>> bOutData = new BData<>();
		 * bOutData.setData( container );
		 * 
		 * out.setData( bOutData ); return out; }
		 */

		// TODO Loop things here

		OutSubscriber outSubscriber = new OutSubscriber();

		container.addBean(outSubscriber);

		out.setStatusCode(1);
		out.setMsg("Data fetch successful.");

		return out;
	}

	@Override
	public Date getExportFDate(InTxn inTxn, Subscriber001Repo repo) {

		int fromPgNo = inTxn.getExportFPgNo();
		log.info("In export F-PgNo " + fromPgNo);
		int excludePgNo = fromPgNo - 1;
		if (fromPgNo <= 1) {
			excludePgNo = 1;
			fromPgNo = 1;
		}

		// - find max date in excludePgNo page of that.
		/*
		Page<Subscriber001> expoExcludePage = repo.findPageByDateRange(
				new Pager().getPageRequest(excludePgNo),
				DateFormatFacRuntime.toDate(inTxn.getfDate()),
				DateFormatFacRuntime.toDateUpperBound(inTxn.gettDate()));*/
		
		Page<Subscriber001> expoExcludePage = null;
		if( expoExcludePage == null )
			return new Date();
		
		Date expoFDate = null;
		int tElements = expoExcludePage.getNumberOfElements();

		if (fromPgNo == 1)
			expoFDate = expoExcludePage.getContent().get(0).getLastUpdate();
		else
			expoFDate = expoExcludePage.getContent().get(tElements - 1)
					.getLastUpdate();
		log.info("Export F-Date?: " + expoFDate.toString());
		return expoFDate;
	}

}
