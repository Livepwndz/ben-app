package com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo;

import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.swifta.bj.mtn.fundamo.portal.bean.OutSubRegPacked;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.Subscriber001;



@Repository
// @Transactional( propagation = Propagation.MANDATORY )
public interface Subscriber001Repo extends JpaRepository< Subscriber001, Long >{
	
	

	String joinStr = " JOIN sub.userAccount001 uAcc "
					 +" JOIN uAcc.systemCode uSysCode "
					 +" JOIN sub.person001 p "
					 +" JOIN p.registrationRequestData001s regData ";
	String conStrDateRange = " sub.lastUpdate BETWEEN :fDate AND :tDate ORDER BY sub.lastUpdate ";
	/*
	public Page< Transaction001 > findByPayerAccountNumber( @Param( "payerAccountNumber" ) String payer, Pageable pageable );
    @Query( "SELECT SUM( t.payeeAmount ) FROM Transaction001 t" )
	public long getTotalPayeeAmount();
    
    @Query( "SELECT t FROM Transaction001 t WHERE t.lastUpdate BETWEEN :fDate AND :tDate" )
	public Page< Transaction001 > findPageByDateRange( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );
	
	*/
	
	

	@Query("SELECT COUNT( sub ) FROM Subscriber001 sub "+joinStr+" WHERE uSysCode.code = 'STS001' ")
	public long countActive();
	
	@Query("SELECT COUNT( sub ) FROM Subscriber001 sub  "+joinStr+"  WHERE uSysCode.code != 'STS001' ")
	public long countOther();
	
	String columns = /*" sub "*/ ""
			+"new com.swifta.bj.mtn.fundamo.portal.bean.OutSubRegPacked("
			+"sub.name, regData.msisdn, regData.idNumber,"
			+"regData.idType, p.dateOfBirth, uSysCode.value, sub.lastUpdate )";
	
	
	
	@Query( "SELECT "+columns+" FROM Subscriber001 sub "+joinStr+" WHERE "+conStrDateRange )
	public Slice< OutSubRegPacked > findPageByDateRangePackedSliced( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

	@Query( "SELECT "+columns+" FROM Subscriber001 sub "+joinStr+" WHERE "+conStrDateRange )
	public Page< OutSubRegPacked > findPageByDateRangePacked( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

	@Query( "SELECT "+columns+" FROM Subscriber001 sub "+joinStr+"  WHERE LOWER( sub.name ) LIKE LOWER( CONCAT('%',:name, '%' ) ) AND "+conStrDateRange )
	public Page< OutSubRegPacked > findByNamePacked( Pageable pageable, @Param( "name" ) String name, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

	@Query( "SELECT "+columns+" FROM Subscriber001 sub "+joinStr+"  WHERE LOWER( sub.name ) LIKE LOWER( CONCAT('%',:name, '%' ) ) AND "+conStrDateRange )
	public Slice< OutSubRegPacked > findByNamePackedSliced( Pageable pageable, @Param( "name" ) String name, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

	
	
	String t = " sub ";
    @Query( "SELECT "+t+" FROM Subscriber001 sub "+joinStr+" WHERE "+conStrDateRange )
	public Slice< Subscriber001 > findPageByDateRangeSliced( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );
    
    @Query( "SELECT "+t+" FROM Subscriber001 sub "+joinStr+" WHERE "+conStrDateRange )
	public Page< Subscriber001 > findPageByDateRange( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

    
    
    @Query( "SELECT sub FROM Subscriber001 sub "+joinStr+"  WHERE LOWER( sub.name ) LIKE LOWER( CONCAT('%',:name, '%' ) ) AND "+conStrDateRange )
	public Page< Subscriber001 > findPageByName( Pageable pageable, @Param( "name" ) String name, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );
	
    
    
    
}
