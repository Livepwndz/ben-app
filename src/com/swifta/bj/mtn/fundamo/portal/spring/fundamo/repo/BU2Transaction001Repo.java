package com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.swifta.bj.mtn.fundamo.portal.bean.OutSubPacked;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.Transaction001;


@Repository
// @Transactional( propagation = Propagation.MANDATORY )
public interface BU2Transaction001Repo extends JpaRepository<Transaction001, Long> {
	
	String joinStr = " JOIN t.entry001s e "
					+" JOIN e.entryType001 eType " 
					+" JOIN e.systemCode eSysCode "
					+" JOIN eType.systemCode eTypeSysCode "
					+" JOIN t.payerAccountIdentifier001s payerAI "
					+" JOIN t.payeeAccountIdentifier001s payeeAI "
					+" JOIN t.transactionType001 tType  "
					+" JOIN t.systemCode tSysCode  "
					+" JOIN tType.systemCode tTypeSysCode ";
	
	String conditionStrDateRange = " ( t.lastUpdate BETWEEN :fDate AND :tDate ) ";
	
	String conditionStrNotNull = " ( t.payerAccountIdentifier001s IS EMPTY OR  payerAI.typeName = 'ACI003' ) "
								+" AND ( t.payeeAccountIdentifier001s IS EMPTY OR  payeeAI.typeName = 'ACI003' ) "
								+" AND e.amount LIKE '-%' "
								+" AND eSysCode.code != 'ETC123' ";
	String conditionStr = conditionStrNotNull+" AND "+  conditionStrDateRange;
	String conditionOrderBy = " ORDER BY t.lastUpdate ";
	
	
	
	String columns = "" 
			+"new com.swifta.bj.mtn.fundamo.portal.bean.OutSubPacked("
			+"t.transactionNumber, tTypeSysCode.value, eSysCode.value, e.amount, tSysCode.value, t.lastUpdate, t.payerAccountNumber, t.payeeAccountNumber, payerAI, payeeAI )";
	
    @Query( "SELECT "+columns
    		+" FROM Transaction001 t "+joinStr+" WHERE "+conditionStr + conditionOrderBy)
	public Slice< OutSubPacked > findPageByDateRangePackedSliced( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );
   
    @Query( "SELECT "+columns
    		+" FROM Transaction001 t "+joinStr+" WHERE "+conditionStr + conditionOrderBy)
	public Page< OutSubPacked > findPageByDateRangePacked( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

    
    String t = " t ";
    @Query( "SELECT "+t
    		+" FROM Transaction001 " +t+joinStr+" WHERE "+conditionStr + conditionOrderBy)
	public Slice< Transaction001 > findPageByDateRangeSliced( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

    // Paged
    @Query( "SELECT "+t
    		+" FROM Transaction001 t "+joinStr+" WHERE "+conditionStr + conditionOrderBy)
	public Page< Transaction001 > findPageByDateRange( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

	
    // Stats
    /*
	@Query("SELECT COUNT( * ) FROM Transaction001 t "+joinStr+" WHERE s.code = 'STS012' ")
	public long countSuccess();
	
	@Query("SELECT COUNT( * ) FROM Transaction001 t "+joinStr+" WHERE s.code != 'STS012' ")
	public long countOther();*/
    
    @Query("SELECT COUNT( t.oid ) FROM Transaction001 t JOIN t.systemCode s WHERE s.code = 'STS012' ")
	public long countSuccess();
	
	@Query("SELECT COUNT( t.oid ) FROM Transaction001 t JOIN t.systemCode s WHERE s.code != 'STS012' ")
	public long countOther();
	
	// Amount 

	@Query("SELECT COALESCE(  SUM( t.payeeAmount ), 0 ) FROM Transaction001 t   "+joinStr+"  WHERE  "+conditionStrNotNull+" AND t.lastUpdate BETWEEN :fDate AND :tDate")
	public double findByDateRangeAmount(@Param("fDate") Date fDate, @Param("tDate") Date tDate);
	
	
	// Search
	
	
	// Transaction no [ page & amount ]
	
	@Query("SELECT t FROM Transaction001 t  "+joinStr+" WHERE "+conditionStrNotNull+" AND t.transactionNumber = :tNo AND "+conditionStrDateRange + conditionOrderBy )
	public Page<Transaction001> findPageByTNo( Pageable pageable, @Param("tNo") BigDecimal tNo );
	
	@Query("SELECT t FROM Transaction001 t  "+joinStr+" WHERE "+conditionStrNotNull+" AND  t.transactionNumber = :tNo AND "+conditionStrDateRange + conditionOrderBy )
	public Page<Transaction001> findPageByTNoSliced( Pageable pageable, @Param("tNo") BigDecimal tNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate );
	
   @Query( "SELECT "+columns
    		+" FROM Transaction001 t "+joinStr+" WHERE "+conditionStrNotNull+" AND t.transactionNumber = :tNo  AND "+conditionStrDateRange + conditionOrderBy )
	public Slice< OutSubPacked > findPageByTNoPackedSliced( Pageable pageable, @Param("tNo") BigDecimal tNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate );
   
   @Query( "SELECT "+columns
   		+" FROM Transaction001 t "+joinStr+" WHERE "+conditionStrNotNull+" AND t.transactionNumber = :tNo AND "+conditionStrDateRange + conditionOrderBy )
	public Page< OutSubPacked > findPageByTNoPacked(Pageable pageable, @Param("tNo") BigDecimal tNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate );
   
   @Query("SELECT COALESCE(  SUM( t.payeeAmount ), 0 ) FROM Transaction001 t   "+joinStr+" WHERE  "+conditionStrNotNull+" AND t.transactionNumber = :tNo AND "+conditionStrDateRange + conditionOrderBy )
	public double findByTNoAmount( @Param("tNo") BigDecimal tNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate );
   
	// Payee [ page & amount ]
	@Query("SELECT "+columns
	   		+" FROM Transaction001 t  "+joinStr+"  WHERE  "+conditionStrNotNull+" AND payeeAI.name LIKE %:payeeAccNo% AND "+conditionStrDateRange + conditionOrderBy )
	public Page<OutSubPacked> findPageByPayeeAccNoPacked(Pageable pageable, @Param("payeeAccNo") String payeeAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);
	
	@Query("SELECT "+columns
	   		+" FROM Transaction001 t  "+joinStr+"  WHERE "+conditionStrNotNull+" AND  payeeAI.name LIKE %:payeeAccNo% AND "+conditionStrDateRange + conditionOrderBy)
	public Slice<OutSubPacked> findPageByPayeeAccNoPackedSliced(Pageable pageable, @Param("payeeAccNo") String payeeAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);

	@Query("SELECT COALESCE(  SUM( t.payeeAmount ), 0 )FROM Transaction001 t   "+joinStr+"  WHERE  "+conditionStrNotNull+" AND payeeAI.name LIKE %:payeeAccNo%  AND "+conditionStrDateRange + conditionOrderBy)
	public double findByPayeeAccNoAmount( @Param("payeeAccNo") String payeeAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);
	
	
	// Payer [ page & amount ]
	@Query("SELECT "+columns
	   		+" FROM Transaction001 t  "+joinStr+"  WHERE  "+conditionStrNotNull+" AND payerAI.name LIKE %:payerAccNo% AND "+conditionStrDateRange + conditionOrderBy)
	public Page<OutSubPacked> findPageByPayerAccNoPacked(Pageable pageable, @Param("payerAccNo") String payerAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);
	
	@Query("SELECT "+columns
	   		+" FROM Transaction001 t  "+joinStr+"  WHERE "+conditionStrNotNull+" AND  payerAI.name LIKE %:payerAccNo% AND "+conditionStrDateRange + conditionOrderBy)
	public Slice<OutSubPacked> findPageByPayerAccNoPackedSliced(Pageable pageable, @Param("payerAccNo") String payeeAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);

	@Query("SELECT COALESCE(  SUM( t.payeeAmount ), 0 )FROM Transaction001 t   "+joinStr+"  WHERE  "+conditionStrNotNull+" AND payerAI.name LIKE %:payerAccNo%  AND "+conditionStrDateRange  + conditionOrderBy)
	public double findByPayerAccNoAmount( @Param("payerAccNo") String payerAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);
	

	
	
	
	
	
	
	
	
	
	
	
	

	/*
	// Payee [ page & amount ]
	@Query("SELECT t FROM Transaction001 t  "+joinStr+" WHERE t.payerAccountNumber LIKE %:payerAccNo%  AND "+conditionStrDateRange)
	public Page<Transaction001> findPageByPayerAccNo(Pageable pageable, @Param("payerAccNo") String payerAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);
	
	@Query("SELECT t FROM Transaction001 t  "+joinStr+" WHERE t.payerAccountNumber LIKE %:payerAccNo%  AND "+conditionStrDateRange)
	public Slice<Transaction001> findPageByPayerAccNoSlice(Pageable pageable, @Param("payerAccNo") String payerAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);

	
	@Query("SELECT COALESCE(  SUM( t.payeeAmount ), 0 ) FROM Transaction001 t   "+joinStr+"  WHERE t.payerAccountNumber LIKE %:payerAccNo% AND "+conditionStrDateRange)
	public double findByPayerAccNoAmount( @Param("payerAccNo") String payerAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate );
	*/

	

}
