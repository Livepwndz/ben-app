package com.swifta.bj.mtn.fundamo.portal.spring.email;

import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.vaadin.spring.annotation.SpringComponent;

@SpringComponent
public class EmailServiceImpl implements IEmailService {

	@Autowired
	public JavaMailSenderImpl emailSender;
	
	private Logger log = LogManager.getLogger();

	public boolean sendSimpleMessage(String to, String subject, String html) {

		String from = "fundamo@mtn.bj";
		try {
			MimeMessage mimeMessage = emailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMessage,
					false, "utf-8");
			
			log.info( "To email: "+to );
			mimeMessage.setContent(html, "text/html");
			helper.setFrom( from );
			helper.setTo(to.trim());
			helper.setSubject(subject);
			emailSender.send(mimeMessage);
			return true;

		} catch (Exception ex) {
			ex.printStackTrace();

		}
		
		return false;

	}
	
	
	
	
	
	
	
	
	
}