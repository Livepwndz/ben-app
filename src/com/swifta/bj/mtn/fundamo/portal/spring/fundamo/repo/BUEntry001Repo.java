package com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.Entry001;



@Repository
// @Transactional( propagation = Propagation.MANDATORY )
public interface BUEntry001Repo extends JpaRepository< Entry001, Long >{
	/*
	
	//@Query( "SELECT e FROM Entry001 e JOIN e.userAccount001 ua JOIN ua.accountIdentifier001 ui  WHERE ui.typeName = :typeName" )
	// public Page< Entry001 > findByUserAccount001AccountIdentifier001TypeName( @Param( "typeName" ) String typeName, Pageable pageable );
	// public Page< Transaction001 > getSubscriberTransactionHistory();
	
	String joinStr = "  JOIN e.transaction001 t JOIN t.payerUserAccount001 payerUA "
					+" JOIN t.payeeUserAccount001 payeeUA  JOIN payeeUA.corporateAccountHolder001 corp";
	
	String conditionStrDateRange = " ( e.entryDate BETWEEN :fDate AND :tDate ) ";
	
	
	String conditionStrNotNull = " t.payerUserAccount001 IS NOT NULL "
								+" AND t.payeeUserAccount001 IS NOT NULL "
								+" AND payerUA.accountIdentifier001s IS NOT EMPTY "
								+" AND payeeUA.accountIdentifier001s IS NOT EMPTY "
								+" AND payeeUA.corporateAccountHolder001 IS NOT NULL ";
								
	
	
	
	
	String conditionStr = conditionStrNotNull+" AND "+  conditionStrDateRange;
	String conditionOrderBy = " ORDER BY e.entryDate ";
	
			
	
	public List< Entry001 > findByOid( long oid );
    @Query( "SELECT e FROM Entry001 e "+joinStr+" WHERE "+conditionStr + conditionOrderBy)
	public Page< Entry001 > findPageByDateRange( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );
    
    @Query( "SELECT COALESCE( SUM( e.amount ), 0 ) FROM Entry001 e "+joinStr+" WHERE "+conditionStr )
	public double findPageByDateRangeAmount( @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

    
    // @Query( "SELECT COUNT( e ) FROM Entry001 e WHERE e.entryDate BETWEEN :fDate AND :tDate" )
	// public long findPageByDateRangeCount( @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );
    
    //  @Query( "SELECT SUM( e.amount ), COUNT( e ) FROM Entry001 e" )
	// public List< Object[] > getTotalAmountAndCountAll();
	
    // @Query( "SELECT SUM( e.amount ), COUNT( e ) FROM Entry001 e WHERE e.entryDate BETWEEN :fDate AND :tDate" )
	// public List< Object[] > findByDateRangeAmountAndCount( @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );
	
	@Query( "SELECT MIN( e.entryDate ) FROM Entry001 e" )
	public Date findEarliestDate();
    
    @Query( "SELECT e FROM Entry001 e "+joinStr+" WHERE e.entryDate = :date AND "+conditionStrNotNull+conditionOrderBy )
	public Page< Entry001 > findPageByDate( Pageable pageable, @Param( "date" ) Date date );
    
    @Query( "SELECT e FROM Entry001 e  "+joinStr+"  WHERE e.entryDate > :date AND "+conditionStrNotNull+conditionOrderBy )
	public Page< Entry001 > findFirstPageByDate( Pageable pageable, @Param( "date" ) Date date );
    
    
   //  @Query( "SELECT COUNT( e ) FROM Entry001 e WHERE e.entryDate = :date" )
	// public long findPageByDateCount( @Param( "date" ) Date date );
    
    
	// Search by something
	
	@Query("SELECT e FROM Entry001 e  "+joinStr+"  WHERE t.payerAccountNumber LIKE %:payerAccNo% AND "+conditionStr +conditionOrderBy)
	public Page<Entry001> findPageByPayerAccountNumber(Pageable pageable, @Param("payerAccNo") String payerAccNo, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate);
	@Query("SELECT COALESCE( SUM( e.amount ), 0 ) FROM Entry001 e  "+joinStr+"  WHERE t.payerAccountNumber LIKE %:payerAccNo% AND "+conditionStr)
	public double findPageByPayerAccountNumberAmount( @Param("payerAccNo") String payerAccNo, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

	@Query("SELECT e FROM Entry001 e  "+joinStr+" WHERE t.payeeAccountNumber LIKE %:payeeAccNo% AND "+conditionStr+conditionOrderBy)
	public Page<Entry001> findPageByPayeeAccountNumber(Pageable pageable, @Param("payeeAccNo") String payeeAccNo, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate);
	@Query("SELECT COALESCE( SUM( e.amount ), 0 ) FROM Entry001 e  "+joinStr+" WHERE t.payeeAccountNumber LIKE %:payeeAccNo% AND "+conditionStr)
	public double findPageByPayeeAccountNumberAmount( @Param("payeeAccNo") String payeeAccNo, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

	@Query("SELECT e FROM Entry001 e  "+joinStr+"  WHERE t.transactionNumber = :tNo AND "+conditionStrNotNull)
	public Page<Entry001> findPageByTransactionNumber( Pageable pageable, @Param("tNo") BigDecimal tNo );
	@Query("SELECT COALESCE( SUM( e.amount ), 0 ) FROM Entry001 e  "+joinStr+"  WHERE t.transactionNumber = :tNo AND "+conditionStrNotNull)
	public double findPageByTransactionNumberAmount( @Param("tNo") BigDecimal tNo );
	
	@Query("SELECT e FROM Entry001 e "+joinStr+" WHERE corp.name LIKE %:name% AND "+conditionStr+conditionOrderBy)
	public Page<Entry001> findPageByCorpName( Pageable pageable, @Param("name") String name, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );
	
	*/
	
}
