package com.swifta.bj.mtn.fundamo.portal.spring.config;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.swifta.bj.mtn.fundamo.portal.controller.main.PortalUI;
import com.swifta.bj.mtn.fundamo.portal.model.admin.Person;

@Configuration()
//@EnableVaadin
@PropertySource( "classpath:application.properties")
@ComponentScan( basePackages = { "com.swifta.bj.mtn.fundamo.portal.spring.*" } )
public class Config {
	
	
	
	
	
	@Bean
	public Person getPerson(){
		return new Person();
	}
	
	
	
	@Bean
	public PortalUI.VaadinSpringConfig getVaadinSpringConfig(){
		return new PortalUI.VaadinSpringConfig();
	}
	
	@Bean
	public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
		return new PropertySourcesPlaceholderConfigurer();
	}
	
	@Bean
	public JavaMailSenderImpl getJavaMailSender() {
	    JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
	    mailSender.setHost("mail.mtn.bj");
	    mailSender.setPort(25);
	     
	    // fundamo@mtn.bj
	    mailSender.setUsername("fundamo");
	    mailSender.setPassword("Discretion@2018");
	    
	     
	    Properties props = mailSender.getJavaMailProperties();
	    props.put("mail.transport.protocol", "smtp");
	    props.put("mail.smtp.auth", "true");
	    props.put("mail.smtp.starttls.enable", "false");
	    props.put("mail.debug", "false");
	    // mailSender.setJavaMailProperties( props );
	    
	   
	    Session mailSession = Session.getInstance( props, new Authenticator() {
		    protected PasswordAuthentication getPasswordAuthentication() {
		       //Fill in your data here.
		       return new PasswordAuthentication("fundamo", "Discretion@2018");
		    }
		});
	    mailSender.setSession( mailSession );
	     
	    return mailSender;
	}

}
