package com.swifta.bj.mtn.fundamo.portal.spring.email;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.vaadin.spring.annotation.SpringComponent;

@SpringComponent
public class EmailServiceImplRaw implements IEmailService {
	
		private Logger log = LogManager.getLogger();

		public boolean sendSimpleMessage( String from, String to, String subject, String html, JavaMailSenderImpl sender ) {

		try {

			String userName = "fundamo";
			String userPassword = "Discretion@2018";
			Properties mailProperties = sender.getJavaMailProperties();
			
			mailProperties.setProperty( "mail.transport.protocol", "smtp" );
			mailProperties.setProperty( "mail.smtp.host", "mail.mtn.bj" );
			mailProperties.setProperty( "mail.smtp.port", "25" );
			mailProperties.setProperty( "mail.smtp.auth", "true" );
			mailProperties.setProperty( "mail.smtp.user",userName );
		    mailProperties.setProperty( "mail.smtp.password", userPassword );
			
			log.info( "Username: "+userName );
			
			
			// Create session
			Session mailSession = null;
			if( null != userName && null != userPassword ) {
				final String name = userName;
				final String pw = userPassword;
				
				Authenticator auth = new Authenticator(){
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(name,pw);
					}
				};
				mailSession = Session.getInstance(mailProperties, auth);
			} else {
				mailSession = Session.getInstance(mailProperties);
			}
			
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(mailSession);
			
			// Set "from" address
			message.setFrom(new InternetAddress( from ));
			
			// Set "to" address
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to.trim()));
			
			// Subject
			message.setSubject("Hello");
			
			// Body
			message.setText("Hello World");
			
			mailSession.setDebug( true );
			
			Transport t = mailSession.getTransport( "smtp" );
			try {
			    t.connect(userName, userPassword);
			    t.sendMessage(message, message.getAllRecipients());
			} finally {
			    t.close();
			}
			
			// Send message
			// Transport.send(message, userName, userPassword);	
			
			
			return true;

		} catch (Exception ex) {
			ex.printStackTrace();

		}

		return false;

	}

		@Override
		public boolean sendSimpleMessage(String to, String subject, String text) {
			// TODO Auto-generated method stub
			return false;
		}
	
	
	
	
	
	
	
	
	
}