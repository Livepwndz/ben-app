package com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.swifta.bj.mtn.fundamo.portal.bean.OutSubPacked;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.Transaction001;


@Repository
// @Transactional( propagation = Propagation.MANDATORY )
public interface Transaction001Repo extends JpaRepository<Transaction001, Long> {
	
	String joinStr = " JOIN t.entry001s e "
					+" JOIN e.entryType001 eType " 
					// +" JOIN e.userAccount001 uAcc "
					// +" LEFT JOIN uAcc.subscriber001 sub "
					// +" LEFT JOIN uAcc.corporateAccountHolder001 corp "
					+" JOIN e.systemCode eSysCode "
					+" JOIN eType.systemCode eTypeSysCode "
					+" JOIN t.payerAccountIdentifier001s payerAI "
					+" JOIN t.payeeAccountIdentifier001s payeeAI "
					
					+" JOIN payerAI.userAccount001 payerUAcc "
					+" JOIN payeeAI.userAccount001 payeeUAcc "
					
					
					+" LEFT JOIN payerUAcc.subscriber001 payerSub "
					+" LEFT JOIN payerUAcc.corporateAccountHolder001 payerCorp "
					
					+" LEFT JOIN payeeUAcc.subscriber001 payeeSub "
					+" LEFT JOIN payeeUAcc.corporateAccountHolder001 payeeCorp "


					+" JOIN t.transactionType001 tType  "
					+" JOIN t.systemCode tSysCode  "
					+" JOIN tType.systemCode tTypeSysCode ";
	
	
	String joinStrLean = " JOIN t.entry001s e "
			+" JOIN e.entryType001 eType " 
			+" JOIN e.systemCode eSysCode "
			+" JOIN eType.systemCode eTypeSysCode "
			+" JOIN t.payerAccountIdentifier001s payerAI "
			+" JOIN t.payeeAccountIdentifier001s payeeAI "
			+" JOIN t.transactionType001 tType  "
			+" JOIN t.systemCode tSysCode  "
			+" JOIN tType.systemCode tTypeSysCode ";
	
	
	String conditionStrDateRange = " ( t.lastUpdate BETWEEN :fDate AND :tDate ) ";
	
	/*
	String conditionStrNotNull = " ( t.payerAccountIdentifier001s IS EMPTY OR  payerAI.typeName = 'ACI003' ) "
								+" AND ( t.payeeAccountIdentifier001s IS EMPTY OR  payeeAI.typeName = 'ACI003' ) "
								+" AND e.amount LIKE '-%' "
								+" AND eSysCode.code != 'ETC123' "; */
	
	
	String conditionStrNotNull = " ( payerAI.typeName = 'ACI003' ) "
			+" AND ( payeeAI.typeName = 'ACI003' ) "
			+" AND e.amount LIKE '-%' "
			+" AND eSysCode.code != 'ETC123' ";
	
	
	
	
	String conditionStr = conditionStrNotNull+" AND "+  conditionStrDateRange;
	String conditionOrderBy = " ORDER BY t.lastUpdate ";
	
	
	
	String columns = "" 
			+"new com.swifta.bj.mtn.fundamo.portal.bean.OutSubPacked("
			+" t.transactionNumber, tTypeSysCode.value, eSysCode.value, e.amount, tSysCode.value, t.lastUpdate, t.payerAccountNumber, t.payeeAccountNumber, "
			+" payerAI.name, payeeAI.name, payerSub.name, payeeSub.name, payerCorp.name, payeeCorp.name )";
	
	String countTurple = "" 
			+" COUNT( t.oid )";

	
    @Query( "SELECT "+columns
    		+" FROM Transaction001 t "+joinStr+" WHERE "+conditionStr + conditionOrderBy)
	public Slice< OutSubPacked > findPageByDateRangePackedSliced( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );
   
    
    @Query( "SELECT "+columns
    		+" FROM Transaction001 t "+joinStr+" WHERE "+conditionStr + conditionOrderBy)
	public Page< OutSubPacked > findPageByDateRangePacked( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

    @Query( "SELECT "+countTurple
    		+" FROM Transaction001 t "+joinStrLean+" WHERE "+conditionStr )
	public Long findPageByDateRangePackedCount( @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

    
    String t = " t ";
    @Query( "SELECT "+t
    		+" FROM Transaction001 " +t+joinStr+" WHERE "+conditionStr + conditionOrderBy)
	public Slice< Transaction001 > findPageByDateRangeSliced( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

    // Paged
    @Query( "SELECT "+t
    		+" FROM Transaction001 t "+joinStr+" WHERE "+conditionStr + conditionOrderBy)
	public Page< Transaction001 > findPageByDateRange( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

	
    // Stats
    /*
	@Query("SELECT COUNT( * ) FROM Transaction001 t "+joinStr+" WHERE s.code = 'STS012' ")
	public long countSuccess();
	
	@Query("SELECT COUNT( * ) FROM Transaction001 t "+joinStr+" WHERE s.code != 'STS012' ")
	public long countOther();*/
    
    @Query("SELECT COUNT( t.oid ) FROM Transaction001 t JOIN t.systemCode s WHERE s.code = 'STS012' ")
	public long countSuccess();
	
	@Query("SELECT COUNT( t.oid ) FROM Transaction001 t JOIN t.systemCode s WHERE s.code != 'STS012' ")
	public long countOther();
	
	// Amount 

	@Query("SELECT COALESCE(  SUM( e.amount ), 0 ) FROM Transaction001 t   "+joinStrLean+"  WHERE  "+conditionStrNotNull+" AND t.lastUpdate BETWEEN :fDate AND :tDate")
	public double findByDateRangeAmount(@Param("fDate") Date fDate, @Param("tDate") Date tDate);
	
	
	// Search
	
	
	// Transaction no [ page & amount ]
	
	@Query("SELECT t FROM Transaction001 t  "+joinStr+" WHERE "+conditionStrNotNull+" AND t.transactionNumber = :tNo AND "+conditionStrDateRange + conditionOrderBy )
	public Page<Transaction001> findPageByTNo( Pageable pageable, @Param("tNo") BigDecimal tNo );
	
	@Query("SELECT t FROM Transaction001 t  "+joinStr+" WHERE "+conditionStrNotNull+" AND  t.transactionNumber = :tNo AND "+conditionStrDateRange + conditionOrderBy )
	public Page<Transaction001> findPageByTNoSliced( Pageable pageable, @Param("tNo") BigDecimal tNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate );
	
   @Query( "SELECT "+columns
    		+" FROM Transaction001 t "+joinStr+" WHERE "+conditionStrNotNull+" AND t.transactionNumber = :tNo  AND "+conditionStrDateRange + conditionOrderBy )
	public Slice< OutSubPacked > findPageByTNoPackedSliced( Pageable pageable, @Param("tNo") BigDecimal tNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate );
   
   @Query( "SELECT COUNT( t.oid ) "
   		+" FROM Transaction001 t "+joinStrLean+" WHERE "+conditionStrNotNull+" AND t.transactionNumber = :tNo  AND "+conditionStrDateRange )
	public Long findPageByTNoPackedSlicedCount( @Param("tNo") BigDecimal tNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate );

   
   @Query( "SELECT "+columns
   		+" FROM Transaction001 t "+joinStr+" WHERE "+conditionStrNotNull+" AND t.transactionNumber = :tNo AND "+conditionStrDateRange + conditionOrderBy )
	public Slice< OutSubPacked > findPageByTNoPacked(Pageable pageable, @Param("tNo") BigDecimal tNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate );
   
   
   @Query("SELECT COALESCE(  SUM( e.amount ), 0 ) FROM Transaction001 t   "+joinStr+" WHERE  "+conditionStrNotNull+" AND t.transactionNumber = :tNo AND "+conditionStrDateRange )
	public double findByTNoAmount( @Param("tNo") BigDecimal tNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate );
   
	// Payee [ page & amount ]
	@Query("SELECT "+columns
	   		+" FROM Transaction001 t  "+joinStr+"  WHERE  "+conditionStrNotNull+" AND payeeAI.name LIKE %:payeeAccNo% AND "+conditionStrDateRange + conditionOrderBy )
	public Slice<OutSubPacked> findPageByPayeeAccNoPacked(Pageable pageable, @Param("payeeAccNo") String payeeAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);
	
	@Query("SELECT "+columns
	   		+" FROM Transaction001 t  "+joinStr+"  WHERE "+conditionStrNotNull+" AND  payeeAI.name LIKE %:payeeAccNo% AND "+conditionStrDateRange + conditionOrderBy)
	public Slice<OutSubPacked> findPageByPayeeAccNoPackedSliced(Pageable pageable, @Param("payeeAccNo") String payeeAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);

	@Query("SELECT COUNT( t.oid )"
	   		+" FROM Transaction001 t  "+joinStrLean+"  WHERE "+conditionStrNotNull+" AND  payeeAI.name LIKE %:payeeAccNo% AND "+conditionStrDateRange)
	public Long findPageByPayeeAccNoPackedSlicedCount( @Param("payeeAccNo") String payeeAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);

	@Query("SELECT COALESCE(  SUM( e.amount ), 0 )FROM Transaction001 t   "+joinStrLean+"  WHERE  "+conditionStrNotNull+" AND payeeAI.name LIKE %:payeeAccNo%  AND "+conditionStrDateRange)
	public double findByPayeeAccNoAmount( @Param("payeeAccNo") String payeeAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);
	
	
	// Payer [ page & amount ]
	@Query("SELECT "+columns
	   		+" FROM Transaction001 t  "+joinStr+"  WHERE  "+conditionStrNotNull+" AND payerAI.name LIKE %:payerAccNo% AND "+conditionStrDateRange + conditionOrderBy)
	public Slice<OutSubPacked> findPageByPayerAccNoPacked(Pageable pageable, @Param("payerAccNo") String payerAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);
	
	@Query("SELECT "+columns
	   		+" FROM Transaction001 t  "+joinStr+"  WHERE "+conditionStrNotNull+" AND  payerAI.name LIKE %:payerAccNo% AND "+conditionStrDateRange + conditionOrderBy)
	public Slice<OutSubPacked> findPageByPayerAccNoPackedSliced(Pageable pageable, @Param("payerAccNo") String payeeAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);

	@Query("SELECT COUNT( t.oid ) "
	   		+" FROM Transaction001 t  "+joinStrLean+"  WHERE "+conditionStrNotNull+" AND  payerAI.name LIKE %:payerAccNo% AND "+conditionStrDateRange )
	public Long findPageByPayerAccNoPackedSlicedCount( @Param("payerAccNo") String payeeAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);

	@Query("SELECT COALESCE(  SUM( e.amount ), 0 )FROM Transaction001 t   "+joinStrLean+"  WHERE  "+conditionStrNotNull+" AND payerAI.name LIKE %:payerAccNo%  AND "+conditionStrDateRange)
	public double findByPayerAccNoAmount( @Param("payerAccNo") String payerAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);
	
	
	

	/*
	// Payee [ page & amount ]
	@Query("SELECT t FROM Transaction001 t  "+joinStr+" WHERE t.payerAccountNumber LIKE %:payerAccNo%  AND "+conditionStrDateRange)
	public Page<Transaction001> findPageByPayerAccNo(Pageable pageable, @Param("payerAccNo") String payerAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);
	
	@Query("SELECT t FROM Transaction001 t  "+joinStr+" WHERE t.payerAccountNumber LIKE %:payerAccNo%  AND "+conditionStrDateRange)
	public Slice<Transaction001> findPageByPayerAccNoSlice(Pageable pageable, @Param("payerAccNo") String payerAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);

	
	@Query("SELECT COALESCE(  SUM( e.amount ), 0 ) FROM Transaction001 t   "+joinStr+"  WHERE t.payerAccountNumber LIKE %:payerAccNo% AND "+conditionStrDateRange)
	public double findByPayerAccNoAmount( @Param("payerAccNo") String payerAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate );
	*/

	

}
