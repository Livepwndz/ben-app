package com.swifta.bj.mtn.fundamo.portal.spring.email;

public interface IResourceLoader {
	String loadAsString( String filename );
}
