package com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.Subscriber001;


@Repository
// @Transactional( propagation = Propagation.MANDATORY )
public interface BUSubscriber001Repo extends JpaRepository< Subscriber001, Long >{
	
	
	String joinStr = " JOIN sub.person001 p ";
	String conStrDateRange = " sub.lastUpdate BETWEEN :fDate AND :tDate ORDER BY sub.lastUpdate ";
	/*
	public Page< Transaction001 > findByPayerAccountNumber( @Param( "payerAccountNumber" ) String payer, Pageable pageable );
    @Query( "SELECT SUM( t.payeeAmount ) FROM Transaction001 t" )
	public long getTotalPayeeAmount();
    
    @Query( "SELECT t FROM Transaction001 t WHERE t.lastUpdate BETWEEN :fDate AND :tDate" )
	public Page< Transaction001 > findPageByDateRange( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );
	
	*/
	
	/*
	
	@Query("SELECT COUNT( * ) FROM Subscriber001 sub JOIN sub.userAccount001 uas JOIN uas.systemCode s WHERE s.code = 'STS001' ")
	public long countActive();
	
	@Query("SELECT COUNT( * ) FROM Subscriber001 sub JOIN sub.userAccount001 uas JOIN uas.systemCode s WHERE s.code != 'STS001' ")
	public long countOther();
	
    @Query( "SELECT sub FROM Subscriber001 sub JOIN sub.person001 p WHERE "+conStrDateRange )
	public Page< Subscriber001 > findPageByDateRange( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );
    
    @Query( "SELECT sub FROM Subscriber001 sub JOIN sub.person001 p WHERE sub.name LIKE %:name% AND "+conStrDateRange )
	public Page< Subscriber001 > findPageByName( Pageable pageable, @Param( "name" ) String name, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );
	*/
}
