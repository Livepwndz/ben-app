package com.swifta.bj.mtn.fundamo.portal.spring.email;

public interface IEmailService {
	boolean sendSimpleMessage( String to, String subject, String text );
}
