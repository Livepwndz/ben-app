package com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo;

import java.math.BigDecimal;
import java.util.Date;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Slice;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import com.swifta.bj.mtn.fundamo.portal.bean.OutMerPacked;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.Entry001;



@Repository
// @Transactional( propagation = Propagation.MANDATORY )
public interface Entry001Repo extends JpaRepository< Entry001, Long >{
	
	String joinStr = " JOIN e.transaction001 t "
			+" JOIN e.entryType001 eType "
			+" JOIN eType.systemCode eSysCode "
			+" JOIN e.userAccount001 uAcc "
			+" JOIN uAcc.corporateAccountHolder001 corp "
			+" JOIN uAcc.accountIdentifier001s ai "
			+" JOIN t.payerAccountIdentifier001s payerAI "
		    +" JOIN t.payeeAccountIdentifier001s payeeAI "
			+" JOIN t.transactionType001 tType  "
			+" JOIN t.systemCode tSysCode  "
			+" JOIN tType.systemCode tTypeSysCode ";

	String conditionStrDateRange = " ( e.lastUpdate BETWEEN :fDate AND :tDate ) ";


	String conditionStrNotNull = " e.entryType001 IS NOT NULL "
							+" AND ( uAcc.accountIdentifier001s IS EMPTY OR  ai.typeName = 'ACI003' ) "
							+" AND ( t.payerAccountIdentifier001s IS EMPTY OR  payerAI.typeName = 'ACI003' ) "
							+" AND ( t.payeeAccountIdentifier001s IS EMPTY OR  payeeAI.typeName = 'ACI003' ) ";
	String conditionStr = conditionStrNotNull+" AND "+  conditionStrDateRange;
	String conditionOrderBy = " ORDER BY e.lastUpdate ";
	
	/*
	String name, String msisdn, BigDecimal tno, String type,
	String amount, String status, String channel, String desc, Date entryDate, AccountIdentifier001 payerAI, 
	AccountIdentifier001 payeeAI */
	
	
	
	String columns = /*" e "*/ ""
		+"new com.swifta.bj.mtn.fundamo.portal.bean.OutMerPacked("
		+"corp.name, t.transactionNumber, eSysCode.value, "
		+"e.amount, tSysCode.value, t.channel, e.description, e.lastUpdate, t.payerAccountNumber, t.payeeAccountNumber, payerAI, payeeAI, ai )";
	
	@Query( "SELECT "+columns
		+" FROM Entry001 e "+joinStr+" WHERE "+conditionStr + conditionOrderBy)
	public Page< OutMerPacked > findPageByDateRangePacked( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );
	
	@Query( "SELECT "+columns
			+" FROM Entry001 e "+joinStr+" WHERE "+conditionStr + conditionOrderBy)
		public Slice< OutMerPacked > findPageByDateRangePackedSliced( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

	
	String t = " e ";
	@Query( "SELECT "+t
		+" FROM Entry001 e "+joinStr+" WHERE "+conditionStr + conditionOrderBy)
	public Page< Entry001 > findPageByDateRange( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

	@Query( "SELECT "+t
		+" FROM Entry001 e "+joinStr+" WHERE "+conditionStr + conditionOrderBy)
	public Slice< Entry001 > findPageByDateRangeSliced( Pageable pageable, @Param( "fDate" ) Date fDate, @Param( "tDate" ) Date tDate );

	@Query("SELECT COALESCE(  SUM( e.amount ), 0 ) FROM Entry001 e   "+joinStr+"  WHERE  "+conditionStrNotNull+" AND e.lastUpdate BETWEEN :fDate AND :tDate")
	public double findByDateRangeAmount(@Param("fDate") Date fDate, @Param("tDate") Date tDate);


	// T. No.
   @Query( "SELECT "+columns
    		+" FROM Entry001 e "+joinStr+" WHERE "+conditionStrNotNull+" AND t.transactionNumber = :tNo  AND "+conditionStrDateRange + conditionOrderBy )
	public Slice< OutMerPacked > findPageByTNoPackedSliced( Pageable pageable, @Param("tNo") BigDecimal tNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate );
   
   @Query( "SELECT "+columns
   		+" FROM Entry001 e "+joinStr+" WHERE "+conditionStrNotNull+" AND t.transactionNumber = :tNo AND "+conditionStrDateRange + conditionOrderBy )
	public Page< OutMerPacked > findPageByTNoPacked(Pageable pageable, @Param("tNo") BigDecimal tNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate );
   
   @Query("SELECT COALESCE(  SUM( e.amount ), 0 ) FROM Entry001 e  "+joinStr+" WHERE  "+conditionStrNotNull+" AND t.transactionNumber = :tNo AND "+conditionStrDateRange + conditionOrderBy )
	public double findByTNoAmount( @Param("tNo") BigDecimal tNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate );
	   

	// Payee [ page & amount ]
	@Query("SELECT "+columns
	   		+" FROM Entry001 e  "+joinStr+"  WHERE  "+conditionStrNotNull+" AND payeeAI.name LIKE %:payeeAccNo% AND "+conditionStrDateRange + conditionOrderBy )
	public Page< OutMerPacked > findPageByPayeeAccNoPacked(Pageable pageable, @Param("payeeAccNo") String payeeAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);
	
	@Query("SELECT "+columns
	   		+" FROM  Entry001 e "+joinStr+"  WHERE "+conditionStrNotNull+" AND  payeeAI.name LIKE %:payeeAccNo% AND "+conditionStrDateRange + conditionOrderBy)
	public Slice< OutMerPacked> findPageByPayeeAccNoPackedSliced(Pageable pageable, @Param("payeeAccNo") String payeeAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);

	@Query("SELECT COALESCE(  SUM( e.amount ), 0 ) FROM Entry001 e  "+joinStr+"  WHERE  "+conditionStrNotNull+" AND payeeAI.name LIKE %:payeeAccNo%  AND "+conditionStrDateRange + conditionOrderBy)
	public double findByPayeeAccNoAmount( @Param("payeeAccNo") String payeeAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);
	
	
	// Payer [ page & amount ]
	@Query("SELECT "+columns
	   		+" FROM Entry001 e "+joinStr+"  WHERE  "+conditionStrNotNull+" AND payerAI.name LIKE %:payerAccNo% AND "+conditionStrDateRange + conditionOrderBy)
	public Page< OutMerPacked > findPageByPayerAccNoPacked(Pageable pageable, @Param("payerAccNo") String payerAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);
	
	@Query("SELECT "+columns
	   		+" FROM Entry001 e "+joinStr+"  WHERE "+conditionStrNotNull+" AND  payerAI.name LIKE %:payerAccNo% AND "+conditionStrDateRange + conditionOrderBy)
	public Slice< OutMerPacked > findPageByPayerAccNoPackedSliced(Pageable pageable, @Param("payerAccNo") String payeeAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);

	@Query("SELECT COALESCE(  SUM( e.amount ), 0 ) FROM Entry001 e  "+joinStr+"  WHERE  "+conditionStrNotNull+" AND payerAI.name LIKE %:payerAccNo%  AND "+conditionStrDateRange  + conditionOrderBy)
	public double findByPayerAccNoAmount( @Param("payerAccNo") String payerAccNo, @Param("fDate") Date fDate, @Param("tDate") Date tDate);
	




}
