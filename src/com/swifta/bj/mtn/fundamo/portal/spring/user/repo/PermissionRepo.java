package com.swifta.bj.mtn.fundamo.portal.spring.user.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

import com.swifta.bj.mtn.fundamo.portal.spring.user.entity.Permission;


@Repository
@Transactional( propagation = Propagation.REQUIRED )
public interface PermissionRepo extends JpaRepository< Permission, Short >{
	Permission findByName( String name );
}
