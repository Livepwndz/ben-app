package com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.CorporateAccountHolder001;


@Repository
// @Transactional( propagation = Propagation.MANDATORY )
public interface CorporateAccountHolder001Repo extends JpaRepository< CorporateAccountHolder001, Long >{
	
	@Query("SELECT COUNT( corp.oid ) FROM CorporateAccountHolder001 corp JOIN corp.userAccount001 ua JOIN ua.systemCode s WHERE s.code = 'STS001' ")
	public long countActive();
	
	@Query("SELECT COUNT( corp.oid ) FROM CorporateAccountHolder001 corp JOIN corp.userAccount001 ua JOIN ua.systemCode s WHERE s.code != 'STS001' ")
	public long countOther();
		
}
