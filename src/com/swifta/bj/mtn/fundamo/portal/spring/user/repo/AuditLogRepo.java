package com.swifta.bj.mtn.fundamo.portal.spring.user.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.annotation.Propagation;

import com.swifta.bj.mtn.fundamo.portal.spring.user.entity.AuditLog;
import com.swifta.bj.mtn.fundamo.portal.spring.user.entity.Permission;
import com.swifta.bj.mtn.fundamo.portal.spring.user.entity.User;



@Repository
@Transactional( propagation = Propagation.REQUIRED )
public interface AuditLogRepo extends JpaRepository< AuditLog, Long >{
	
	@Query( "SELECT p FROM AuditLog aLog JOIN aLog.permission p WHERE p.id = :permId" )
	public Permission findByPermissionId( @Param( "permId" ) short permId );
	
	@Query( "SELECT u FROM AuditLog aLog JOIN aLog.user u WHERE u.userId = :userId" )
	public User findByUserUserId( @Param( "userId" ) long userId );
}
