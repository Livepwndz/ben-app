package com.swifta.bj.mtn.fundamo.portal.bean;

public interface IDataBean {
	
	String getColumn1( );
	String getColumn2( );
	String getColumn3( );
	String getColumn4( );
	String getColumn5( );
	String getColumn6( );
	String getColumn7( );
	String getColumn8( );
	String getColumn90( );
	String getColumn91( );
	String getColumn92( );
	String getColumn93( );
	
}
