package com.swifta.bj.mtn.fundamo.portal.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import com.swifta.bj.mtn.fundamo.portal.model.util.DateFormatFacRuntime;
import com.swifta.bj.mtn.fundamo.portal.model.util.NumberFormatFac;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.AccountIdentifier001;

public class OutMerPacked extends AbstractDataBean implements Serializable {
	
	private static final long serialVersionUID = 7431320759077668024L;
	private String name, msisdn,  tno, type, amount, sAmount,  status, channel, desc, payer, payee, payerAccNo, payeeAccNo, entryDate ;
	private AccountIdentifier001 payerAI = new AccountIdentifier001();
	private AccountIdentifier001 payeeAI = new AccountIdentifier001();
	private AccountIdentifier001 ai = new AccountIdentifier001();
	
	
	
	
	
	
	
	
	public OutMerPacked(String name, BigDecimal tno, String type,
			double amount, String status, String channel, String desc, Date entryDate, String payerAccNo, String payeeAccNo, AccountIdentifier001 payerAI, 
			AccountIdentifier001 payeeAI, AccountIdentifier001 ai ) {
		super();
		
		// MSISDN Handling
		if( payerAI != null)
			this.payerAI = payerAI;
		else
			this.payerAI.setName( "No MSISDN" );
		if( payeeAI != null)
			this.payeeAI = payeeAI;
		else
			this.payeeAI.setName( "No MSISDN" );
		
		if( ai != null)
			this.ai = ai;
		else
			this.ai.setName( "No MSISDN" );
		
		
		// this.name = name;
		// this.msisdn = msisdn;
		// this.tno = tno.toString();
		// this.type = type;
		// this.amount = amount;
		// this.status = status;
		// this.channel = channel;
		// this.desc = desc;
		// this.payer = payer;
		// this.payee = payee;
		
		this.setName(name);
		this.setMsisdn(ai.getName());
		this.setTno( tno.toString() );
		this.setType( type );
		this.setsAmount(( amount/100 ) +"");
		this.setAmount( NumberFormatFac.toMoney( this.getsAmount() ) );
		this.setStatus(  status );
		this.setChannel(channel);
		this.setDesc( desc );
		this.setPayer( this.payerAI.getName() );
		this.setPayee( this.payeeAI.getName() );
		this.setPayerAccNo( payerAccNo );
		this.setPayeeAccNo( payeeAccNo );
		
		this.setEntryDate( DateFormatFacRuntime.toString( entryDate ) );
		
	}
	
	
	
	
	public String getsAmount() {
		return sAmount;
	}
	public void setsAmount(String sAmount) {
		this.sAmount = sAmount;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.column1 = name;
		this.name = name;
	}
	public String getMsisdn() {
		return msisdn;
	}
	public void setMsisdn(String msisdn) {
		this.column2 = msisdn;
		this.msisdn = msisdn;
	}
	public String getTno() {
		return tno;
	}
	public void setTno(String tno) {
		this.column3 = tno;
		this.tno = tno;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.column4 = type;
		this.type = type;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.column5 = amount;
		this.amount = amount;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.column6 = status;
		this.status = status;
	}
	public String getChannel() {
		return channel;
	}
	public void setChannel(String channel) {
		this.column7 = channel;
		this.channel = channel;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.column8 = desc;
		this.desc = desc;
	}

	public String getPayer() {
		return payer;
	}
	public void setPayer(String payer) {
		this.column90 = payer;
		this.payer = payer;
	}
	
	public String getPayee() {
		return payee;
	}
	public void setPayee(String payee) {
		this.column91 = payee;
		this.payee = payee;
	}
	
	
	public String getPayerAccNo() {
		return payerAccNo;
	}
	public void setPayerAccNo(String payerAccNo) {
		this.column92 = payerAccNo;
		this.payerAccNo = payerAccNo;
	}
	
	public String getPayeeAccNo() {
		return payeeAccNo;
	}
	public void setPayeeAccNo(String payeeAccNo) {
		this.column93 = payeeAccNo;
		this.payeeAccNo = payeeAccNo;
	}
	
	public String getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(String entryDate) {
		super.date = entryDate;
		this.entryDate = entryDate;
	}
	
	
	
	

	
	
	
	
	
	
	
	
	
	

}
