package com.swifta.bj.mtn.fundamo.portal.bean;

public class OutUserExport extends AbstractDataBean {
	
	private static final long serialVersionUID = 7431320759077668024L;
	private String firstName, username, date;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
	

}
