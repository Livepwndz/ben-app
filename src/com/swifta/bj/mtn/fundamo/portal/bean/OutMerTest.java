package com.swifta.bj.mtn.fundamo.portal.bean;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.Set;

import com.swifta.bj.mtn.fundamo.portal.model.util.DateFormatFacRuntime;
import com.swifta.bj.mtn.fundamo.portal.model.util.NumberFormatFac;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.AccountIdentifier001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.CorporateAccountHolder001;


public class OutMerTest extends AbstractDataBean {
	
	private static final long serialVersionUID = 7431320759077668024L;
	private String transactionNumber, type, amount, status, payer, payee, date;
	private AccountIdentifier001 payerAI = new AccountIdentifier001();
	private AccountIdentifier001 payeeAI = new AccountIdentifier001();
	private CorporateAccountHolder001 corp = new CorporateAccountHolder001();
	
	
	
	
	public OutMerTest(BigDecimal transactionNumber, String type, double amount,
			String status, String payer, String payee, Date date, Set< AccountIdentifier001 > ais ) {
		super();
		this.transactionNumber = transactionNumber.toString();
		this.type = type;
		this.amount = NumberFormatFac.toMoney( amount+"" );
		this.status = status;
		AccountIdentifier001 ai = ais.iterator().next();
		this.payer = ai.getName();
		this.payee = ais.size()+"";
		this.date = DateFormatFacRuntime.toString( date );
		
		
	}
	
	
	public OutMerTest(BigDecimal transactionNumber, String type, double amount,
			String status, String payer, String payee, Date date, AccountIdentifier001 payerAI ) {
		super();
		this.transactionNumber = transactionNumber.toString();
		this.type = type;
		this.amount = NumberFormatFac.toMoney( amount+"" );
		this.status = status;
		this.payerAI = payerAI;
		this.payer = payerAI.getName();
		this.payee = payerAI.getTypeName();
		this.date = DateFormatFacRuntime.toString( date );
	}
	
	
	public OutMerTest(BigDecimal transactionNumber, String type, double amount,
			String status, Date date, AccountIdentifier001 payerAI ) {
		super();
		
		this.transactionNumber = transactionNumber.toString();
		this.type = type;
		this.amount = NumberFormatFac.toMoney( amount+"" );
		this.status = status;
		if( payerAI != null)
			this.payerAI = payerAI;
		this.payer = payerAI.getName();
		this.payee = payerAI.getTypeName();
		this.date = DateFormatFacRuntime.toString( date );
	}
	
	
	public OutMerTest(BigDecimal transactionNumber, String type, double amount,
			String status, Date date, AccountIdentifier001 payerAI, AccountIdentifier001 payeeAI ) {
		super();
		
		this.transactionNumber = transactionNumber.toString();
		this.type = type;
		this.amount = NumberFormatFac.toMoney( amount+"" );
		this.status = status;
		
		if( payerAI != null)
			this.payerAI = payerAI;
		if( payeeAI != null)
			this.payeeAI = payeeAI;
		
		this.payer = payerAI.getName();
		this.payee = payeeAI.getName();
		this.date = DateFormatFacRuntime.toString( date );
	}
	
	
	public OutMerTest( CorporateAccountHolder001 corp, AccountIdentifier001 payerAI, AccountIdentifier001 payeeAI ) {
		super();
		
		if( corp != null )
			this.corp = corp;
		if( payerAI != null)
			this.payerAI = payerAI;
		if( payeeAI != null)
			this.payeeAI = payeeAI;
		
		this.transactionNumber = corp.getName();
		this.payer = payerAI.getName();
		this.payee = payeeAI.getName();
		
	}
	
	
	public OutMerTest(BigDecimal transactionNumber, String type, double amount,
			String status, String payer, String payee, Date date ) {
		super();
		this.transactionNumber = transactionNumber.toString();
		this.type = type;
		this.amount = NumberFormatFac.toMoney( amount+"" );
		this.status = status;
		this.payer = payer;
		this.payee = payee;
		this.date = DateFormatFacRuntime.toString( date );
	}
	
	
	
	
	
	
	

	


	public AccountIdentifier001 getPayerAI() {
		return payerAI;
	}


	public void setPayerAI(AccountIdentifier001 payerAI) {
		this.payerAI = payerAI;
	}


	public String getType() {
		return type;
	}
	public String getTransactionNumber() {
		return transactionNumber;
	}
	public void setTransactionNumber( String transactionNumber ) {
		this.transactionNumber = transactionNumber;
		this.column1 = transactionNumber;
	}
	
	public String getType(String type) {
		return this.type;
	}
	
	public void setType(String type) {
		this.type = type;
		this.column2 = type;
	}
	
	public String getAmount() {
		return amount;
	}
	
	public void setAmount(String amount) {
		this.column3 = amount;
		this.amount = amount;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.column4 = status;
		this.status = status;
	}
	public String getPayer() {
		return payer;
	}
	public void setPayer(String payer) {
		this.column5 = payer;
		this.payer = payer;
	}
	public String getPayee() {
		return payee;
	}
	public void setPayee(String payee) {
		this.column6 = payee;
		this.payee = payee;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		super.date = date;
		this.date = date;
	}


	public AccountIdentifier001 getPayeeAI() {
		return payeeAI;
	}


	public void setPayeeAI(AccountIdentifier001 payeeAI) {
		this.payeeAI = payeeAI;
	}
	
	
	
	
	

	
	
	
	
	
	
	
	
	
	

}
