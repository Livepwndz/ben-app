package com.swifta.bj.mtn.fundamo.portal.bean;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.swifta.bj.mtn.fundamo.portal.model.util.DateFormatFacRuntime;
import com.swifta.bj.mtn.fundamo.portal.model.util.NumberFormatFac;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.AccountIdentifier001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.CorporateAccountHolder001;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.entity.Subscriber001;

public class OutSubPacked extends AbstractDataBean {
	
	private static final long serialVersionUID = 7431320759077668024L;
	
	private Logger log = LogManager.getLogger();
	private String transactionNumber, type, eType, amount, sAmount, status, payer, payee, payerName, payerAccNo, payeeName, payeeAccNo, date;
	private Double dAmount;
	private AccountIdentifier001 payerAI = new AccountIdentifier001();
	private AccountIdentifier001 payeeAI = new AccountIdentifier001();
	
	/*
	private Subscriber001 payerSub, payeeSub = new  Subscriber001();
	private CorporateAccountHolder001 payerCorp, payeeCorp = new CorporateAccountHolder001();
	*/
	
	
	
	
	public OutSubPacked(BigDecimal transactionNumber, String type, double amount,
			String status, String payer, String payee, Date date, Set< AccountIdentifier001 > ais ) {
		super();
		this.transactionNumber = transactionNumber.toString();
		this.type = type;
		this.amount = NumberFormatFac.toMoney( amount+"" );
		this.status = status;
		AccountIdentifier001 ai = ais.iterator().next();
		this.payer = ai.getName();
		this.payee = ais.size()+"";
		this.date = DateFormatFacRuntime.toString( date );
		
		
	}
	
	
	public OutSubPacked(BigDecimal transactionNumber, String type, double amount,
			String status, String payer, String payee, Date date, AccountIdentifier001 payerAI ) {
		super();
		this.transactionNumber = transactionNumber.toString();
		this.type = type;
		this.amount = NumberFormatFac.toMoney( amount+"" );
		this.status = status;
		this.payerAI = payerAI;
		this.payer = payerAI.getName();
		this.payee = payerAI.getTypeName();
		this.date = DateFormatFacRuntime.toString( date );
	}
	
	
	public OutSubPacked(BigDecimal transactionNumber, String type, double amount,
			String status, Date date, AccountIdentifier001 payerAI ) {
		super();
		
		this.transactionNumber = transactionNumber.toString();
		this.type = type;
		this.amount = NumberFormatFac.toMoney( amount+"" );
		this.status = status;
		if( payerAI != null) {
			this.payerAI = payerAI;
		} else {
			this.payerAI.setName( "No MSISDN" );
		}
		this.payer = payerAI.getName();
		this.payee = payerAI.getTypeName();
		this.date = DateFormatFacRuntime.toString( date );
	}
	
	
	public OutSubPacked(BigDecimal transactionNumber, String type, double amount,
			String status, String payer, String payee, Date date ) {
		super();
		this.transactionNumber = transactionNumber.toString();
		this.type = type;
		this.amount = NumberFormatFac.toMoney( amount+"" );
		this.status = status;
		this.payer = payer;
		this.payee = payee;
		this.date = DateFormatFacRuntime.toString( date );
	}
	
	
	public OutSubPacked(BigDecimal transactionNumber, String type, String eType, double amount,
			String status, Date date, String payerAccNo, String payeeAccNo,  AccountIdentifier001 payerAI, AccountIdentifier001 payeeAI ) {
		super();
		
		// MSISDN Handling
		if( payerAI != null)
			this.payerAI = payerAI;
		else
			this.payerAI.setName( "No MSISDN" );
		
		
		if( payeeAI != null)
			this.payeeAI = payeeAI;
		else
			this.payeeAI.setName( "No MSISDN" );
		
		
		this.setTransactionNumber( transactionNumber.toString() );
		this.setType( type );
		this.seteType(eType);
		this.setsAmount(( amount/100 ) +"");
		this.setAmount( NumberFormatFac.toMoney( this.getsAmount() ) );
		this.setStatus(  status );
		this.setPayer( this.payerAI.getName() );
		this.setPayee( this.payeeAI.getName() );
		
		
		
		this.setPayerAccNo( payerAccNo );
		this.setPayeeAccNo( payeeAccNo );
		this.setDate( DateFormatFacRuntime.toString( date ) );
	}
	
	

	
	public OutSubPacked(BigDecimal transactionNumber, String type, String eType, double amount,
			String status, Date date, String payerAccNo, String payeeAccNo
			,AccountIdentifier001 payerAI, AccountIdentifier001 payeeAI
			,Subscriber001 payerSub, Subscriber001 payeeSub, CorporateAccountHolder001 payerCorp, CorporateAccountHolder001 payeeCorp ) {
		super();
		
		// MSISDN Handling
		if( payerAI != null)
			this.payerAI = payerAI;
		else
			this.payerAI.setName( "No MSISDN" );
		
		if( payeeAI != null)
			this.payeeAI = payeeAI;
		else
			this.payeeAI.setName( "No MSISDN" );
		
		
		String payerName, payeeName = "";
		
		
		
		if( payerSub != null ) {
			// this.payerSub = payerSub;
			payerName = payerSub.getName();
			// log.info( "PayerSub. Name: "+payerSub.getName(), this );
		} else {
			// this.payerCorp = payerCorp;
			payerName = payerCorp.getName();
			
		}
		
		if( payeeSub != null ) {
			// this.payeeSub = payeeSub;
			payeeName = payeeSub.getName();
			// log.info( "PayeeSub. Name: "+payeeSub.getName(), this );
		} else {
			// this.payeeCorp = payeeCorp;
			payeeName = payeeCorp.getName();
			
		}
		
		log.info( "Payer. Name: "+payerName, this );
		log.info( "Payee. Name: "+payeeName, this );
		
		
		this.setTransactionNumber( transactionNumber.toString() );
		this.setType( type );
		this.seteType(eType);
		this.setsAmount(( amount/100 ) +"");
		this.setdAmount( amount/100 );
		this.setAmount( NumberFormatFac.toMoney( this.getsAmount() ) );
		this.setStatus(  status );
		this.setPayer( this.payerAI.getName() );
		this.setPayee( this.payeeAI.getName() );
		
		this.setPayerName(payerName);
		this.setPayeeName(payeeName);
		
		this.setPayerAccNo( payerAccNo );
		this.setPayeeAccNo( payeeAccNo );
		this.setDate( DateFormatFacRuntime.toString( date ) );
	}
	
	
	public OutSubPacked(BigDecimal transactionNumber, String type, String eType, double amount,
			String status, Date date, String payerAccNo, String payeeAccNo
			,String payerMsisdn, String payeeMsisdn
			,Subscriber001 payerSub, Subscriber001 payeeSub, CorporateAccountHolder001 payerCorp, CorporateAccountHolder001 payeeCorp ) {
		super();
		
		// MSISDN Handling
		
		/*
		if( payerAI != null)
			this.payerAI = payerAI;
		else
			this.payerAI.setName( "No MSISDN" );
		
		if( payeeAI != null)
			this.payeeAI = payeeAI;
		else
			this.payeeAI.setName( "No MSISDN" ); */
		
		
		String payerName, payeeName = "";
		
		
		
		if( payerSub != null ) {
			// this.payerSub = payerSub;
			payerName = payerSub.getName();
			// log.info( "PayerSub. Name: "+payerSub.getName(), this );
		} else {
			// this.payerCorp = payerCorp;
			payerName = payerCorp.getName();
			
		}
		
		if( payeeSub != null ) {
			// this.payeeSub = payeeSub;
			payeeName = payeeSub.getName();
			// log.info( "PayeeSub. Name: "+payeeSub.getName(), this );
		} else {
			// this.payeeCorp = payeeCorp;
			payeeName = payeeCorp.getName();
			
		}
		
		log.info( "Payer. Name: "+payerName, this );
		log.info( "Payee. Name: "+payeeName, this );
		
		
		this.setTransactionNumber( transactionNumber.toString() );
		this.setType( type );
		this.seteType(eType);
		this.setsAmount(( amount/100 ) +"");
		this.setAmount( NumberFormatFac.toMoney( this.getsAmount() ) );
		this.setStatus(  status );
		this.setPayer( payerMsisdn );
		this.setPayee( payeeMsisdn );
		
		this.setPayerName(payerName);
		this.setPayeeName(payeeName);
		
		this.setPayerAccNo( payerAccNo );
		this.setPayeeAccNo( payeeAccNo );
		this.setDate( DateFormatFacRuntime.toString( date ) );
	}
	
	
	
	public OutSubPacked(BigDecimal transactionNumber, String type, String eType, double amount,
			String status, Date date, String payerAccNo, String payeeAccNo
			,String payerMsisdn, String payeeMsisdn
			,String payerSubName, String payeeSubName, String payerCorpName, String payeeCorpName ) {
		super();
		
		// MSISDN Handling
		
		/*
		if( payerAI != null)
			this.payerAI = payerAI;
		else
			this.payerAI.setName( "No MSISDN" );
		
		if( payeeAI != null)
			this.payeeAI = payeeAI;
		else
			this.payeeAI.setName( "No MSISDN" ); */
		
		
		String payerName, payeeName = "";
		
		
		
		if( payerSubName != null ) {
			// this.payerSub = payerSub;
			payerName = payerSubName;//payerSub.getName();
			// log.info( "PayerSub. Name: "+payerSub.getName(), this );
		} else {
			// this.payerCorp = payerCorp;
			payerName = payerCorpName;
			
			
		}
		
		if( payeeSubName != null ) {
			// this.payeeSub = payeeSub;
			payeeName = payeeSubName;//.getName();
			// log.info( "PayeeSub. Name: "+payeeSub.getName(), this );
		} else {
			// this.payeeCorp = payeeCorp;
			payeeName = payeeCorpName;
			
		}
		
		// log.info( "Payer. Name: "+payerName, this );
		// log.info( "Payee. Name: "+payeeName, this );
		
		
		this.setTransactionNumber( transactionNumber.toString() );
		this.setType( type );
		this.seteType(eType);
		
		this.setsAmount(( amount/100 ) +"");
		this.setdAmount( amount/100 );
		this.setAmount( NumberFormatFac.toMoney( this.getsAmount() ) );
		this.setStatus(  status );
		this.setPayer( payerMsisdn );
		this.setPayee( payeeMsisdn );
		
		this.setPayerName(payerName);
		this.setPayeeName(payeeName);
		
		this.setPayerAccNo( payerAccNo );
		this.setPayeeAccNo( payeeAccNo );
		this.setDate( DateFormatFacRuntime.toString( date ) );
	}
	
	
	
	
	public OutSubPacked(BigDecimal transactionNumber, String type, String eType, double amount,
			String status, Date date, String payerAccNo, String payeeAccNo
			,String payerMsisdn, String payeeMsisdn
			,String payerSubMsisdn, String payeeSubMsisdn, CorporateAccountHolder001 payerCorp, CorporateAccountHolder001 payeeCorp ) {
		super();
		
		// MSISDN Handling
		
		/*
		if( payerAI != null)
			this.payerAI = payerAI;
		else
			this.payerAI.setName( "No MSISDN" );
		
		if( payeeAI != null)
			this.payeeAI = payeeAI;
		else
			this.payeeAI.setName( "No MSISDN" ); */
		
		
		String payerName, payeeName = "";
		
		
		
		if( payerSubMsisdn != null ) {
			// this.payerSub = payerSub;
			payerName = payerSubMsisdn;//payerSub.getName();
			// log.info( "PayerSub. Name: "+payerSub.getName(), this );
		} else {
			// this.payerCorp = payerCorp;
			payerName = payerCorp.getName();
			
			
		}
		
		if( payeeSubMsisdn != null ) {
			// this.payeeSub = payeeSub;
			payeeName = payeeSubMsisdn;//.getName();
			// log.info( "PayeeSub. Name: "+payeeSub.getName(), this );
		} else {
			// this.payeeCorp = payeeCorp;
			payeeName = payeeCorp.getName();
			
		}
		
		log.info( "Payer. Name: "+payerName, this );
		log.info( "Payee. Name: "+payeeName, this );
		
		
		this.setTransactionNumber( transactionNumber.toString() );
		this.setType( type );
		this.seteType(eType);
		
		this.setdAmount( amount/100 );
		
		this.setsAmount(( amount/100 ) +"");
		this.setAmount( NumberFormatFac.toMoney( this.getsAmount() ) );
		this.setStatus(  status );
		this.setPayer( payerMsisdn );
		this.setPayee( payeeMsisdn );
		
		this.setPayerName(payerName);
		this.setPayeeName(payeeName);
		
		this.setPayerAccNo( payerAccNo );
		this.setPayeeAccNo( payeeAccNo );
		this.setDate( DateFormatFacRuntime.toString( date ) );
	}
	
	
	
	
	
	
	
	
	
	

	

	

	public String getsAmount() {
		return sAmount;
	}


	public void setsAmount(String sAmount) {
		this.sAmount = sAmount;
	}


	public AccountIdentifier001 getPayerAI() {
		return payerAI;
	}


	public void setPayerAI(AccountIdentifier001 payerAI) {
		this.payerAI = payerAI;
	}


	public String getType() {
		return type;
	}
	public String getTransactionNumber() {
		return transactionNumber;
	}
	public void setTransactionNumber( String transactionNumber ) {
		this.transactionNumber = transactionNumber;
		this.column1 = transactionNumber;
	}
	
	public String getType(String type) {
		return this.type;
	}
	
	public void setType(String type) {
		this.type = type;
		this.column2 = type;
	}
	
	
	
	public String geteType() {
		return eType;
	}


	public void seteType(String eType) {
		this.column3 = eType;
		this.eType = eType;
	}


	public String getAmount() {
		return amount;
	}
	
	public void setAmount(String amount) {
		this.column4 = amount;
		this.amount = amount;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.column5 = status;
		this.status = status;
	}
	public String getPayer() {
		return payer;
	}
	public void setPayer(String payer) {
		this.column6 = payer;
		this.payer = payer;
	}
	public String getPayee() {
		return payee;
	}
	public void setPayee(String payee) {
		this.column7 = payee;
		this.payee = payee;
	}

	public String getPayerAccNo() {
		return payerAccNo;
	}
	
	public void setPayerAccNo(String payerAccNo) {
		this.column8 = payerAccNo;
		this.payerAccNo = payerAccNo;
	}
	public String getPayeeAccNo() {
		return payeeAccNo;
	}
	public void setPayeeAccNo(String payeeAccNo) {
		this.column90 = payeeAccNo;
		this.payeeAccNo = payeeAccNo;
	}
	
	public String getPayerName() {
		return payerName;
	}
	
	public void setPayerName(String payerName) {
		this.column91 = payerName;
		this.payerName = payerName;
	}
	
	public String getPayeeName() {
		return payeeName;
	}
	
	public void setPayeeName(String payeeName) {
		this.column92 = payeeName;
		this.payeeName = payeeName;
	}
	
	


	public Double getdAmount() {
		return dAmount;
	}


	public void setdAmount(Double dAmount) {
		this.dAmount = dAmount;
	}


	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		super.date = date;
		this.date = date;
	}
	
	
	

	
	
	
	
	
	
	
	
	
	

}
