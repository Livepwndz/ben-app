package com.swifta.bj.mtn.fundamo.portal.bean;

import com.vaadin.data.Item;

public class InUserDetails {
	
	private String username;
	private String password;
	private String userSession;
	private Item record;
	private long userId;
	private short profileId;
	
	
	
	
	public short getProfileId() {
		return profileId;
	}
	public void setProfileId(short profileId) {
		this.profileId = profileId;
	}
	public long getUserId() {
		return userId;
	}
	public void setUserId(long userId) {
		this.userId = userId;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Item getRecord() {
		return record;
	}
	public void setRecord(Item record) {
		this.record = record;
	}
	public String getUserSession() {
		return userSession;
	}
	public void setUserSession(String userSession) {
		this.userSession = userSession;
	}
	
	
	
	
	
	
}
