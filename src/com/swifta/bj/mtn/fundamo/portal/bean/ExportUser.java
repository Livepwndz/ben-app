package com.swifta.bj.mtn.fundamo.portal.bean;

import java.io.Serializable;

public class ExportUser implements Serializable {
	
	private static final long serialVersionUID = 7431320759077668024L;
	// private String username, email, lastLogin, org, userStatus, profile, date;
	
	 private String column1, column2, column3, column4, column5, column6, date;

	public String getColumn1() {
		return column1;
	}

	public void setColumn1(String column1) {
		this.column1 = column1;
	}

	public String getColumn2() {
		return column2;
	}

	public void setColumn2(String column2) {
		this.column2 = column2;
	}

	public String getColumn3() {
		return column3;
	}

	public void setColumn3(String column3) {
		this.column3 = column3;
	}

	public String getColumn4() {
		return column4;
	}

	public void setColumn4(String column4) {
		this.column4 = column4;
	}

	public String getColumn5() {
		return column5;
	}

	public void setColumn5(String column5) {
		this.column5 = column5;
	}

	public String getColumn6() {
		return column6;
	}

	public void setColumn6(String column6) {
		this.column6 = column6;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
	 
	 
	 
	
	
	

}
