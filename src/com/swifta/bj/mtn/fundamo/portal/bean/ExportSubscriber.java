package com.swifta.bj.mtn.fundamo.portal.bean;

import java.io.Serializable;

public class ExportSubscriber implements Serializable {
	
	private static final long serialVersionUID = 7431320759077668024L;
	
	// Weird naming for mapper & export column order reconciliation.
	//private String transactionNumber, type, amount, status, payer, payee, date;
	
	private String column1, column2, column3, column4, column5, column6, column7, column8, column90, column91, column92, date;

	public String getColumn1() {
		return column1;
	}

	public void setColumn1(String column1) {
		this.column1 = column1;
	}

	public String getColumn2() {
		return column2;
	}

	public void setColumn2(String column2) {
		this.column2 = column2;
	}

	public String getColumn3() {
		return column3;
	}

	public void setColumn3(String column3) {
		this.column3 = column3;
	}

	public String getColumn4() {
		return column4;
	}

	public void setColumn4(String column4) {
		this.column4 = column4;
	}

	public String getColumn5() {
		return column5;
	}

	public void setColumn5(String column5) {
		this.column5 = column5;
	}

	public String getColumn6() {
		return column6;
	}

	public void setColumn6(String column6) {
		this.column6 = column6;
	}
	
	

	public String getColumn7() {
		return column7;
	}

	public void setColumn7(String column7) {
		this.column7 = column7;
	}

	public String getColumn8() {
		return column8;
	}

	public void setColumn8(String column8) {
		this.column8 = column8;
	}
	
	

	public String getColumn90() {
		return column90;
	}

	public void setColumn90(String column90) {
		this.column90 = column90;
	}
	
	

	public String getColumn91() {
		return column91;
	}

	public void setColumn91(String column91) {
		this.column91 = column91;
	}

	public String getColumn92() {
		return column92;
	}

	public void setColumn92(String column92) {
		this.column92 = column92;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	
	
	

	
	
	

	
}
