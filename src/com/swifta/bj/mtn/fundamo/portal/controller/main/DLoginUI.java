package com.swifta.bj.mtn.fundamo.portal.controller.main;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.UI;

@SpringComponent
@UIScope
public class DLoginUI extends DUI {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private DLoginUIController dLogin;
	
	
	DLoginUI(){
		init();
	}
		

	@Override
	public void setContent() {
		// dLogin = new DLoginUIController( springAppContext );
		UI.getCurrent().setContent(dLogin);
	}
	
	@Override
	public void init(){
		setContent();
		setLoginUIC(dLogin);
		setFooter();
		
	}
	
	
	
	public DLoginUIController getLoginUIC(){
		return dLogin;
	}
	
	public void setLoginUIC(DLoginUIController dLogin){
		this.dLogin = dLogin;
	}
	


	


}
