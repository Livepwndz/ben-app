package com.swifta.bj.mtn.fundamo.portal.controller.main;

public interface DUIControllable {
	void attachCommandListeners();
	void init();
	void setUIState();
}
