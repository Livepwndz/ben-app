package com.swifta.bj.mtn.fundamo.portal.controller.util;

public class TFNewPasswordValidator extends TFUserPasswordUtil {

	private static final long serialVersionUID = 1L;
	
	public TFNewPasswordValidator(String errorMessage){
		super( errorMessage );
	}
	

	@Override
	protected boolean isValidValue(String value) {
	
			boolean status = true;
			String msg = "";
			if( value == null || value.trim().isEmpty() ){
				return false;
			}else {
				status = value.matches( "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=])(?=\\S+$).{8,}$");
				if( !status ){
					msg = "Password should be 8 characters minimum, aleast a number, upper case letter and atleast one of !@#$%^&+=.";
				}
			}
			
			if( status ) {
				
				Object obj = otherFieldDS.getValue();
				if( obj == null )
					obj = "";
				status = checkPasswordMatch( value, obj.toString() );
				if( !status ){
					msg = "New Password and Confirm Password fields don't match.";
					if( isInitCalled ){
						lbErrorMsg.removeStyleName("sn-display-none");
						lbNormalMsg.addStyleName("sn-display-none");
					}
				}else{
					if( isInitCalled ){
						lbNormalMsg.removeStyleName("sn-display-none");
						lbErrorMsg.addStyleName("sn-display-none");
						
					}
				}
			}
			
			if( !status )
				this.setErrorMessage( msg );
		
		return status;
	}
	
	
	private boolean checkPasswordMatch( String pass, String otherPass ){
		return pass.trim().equals( otherPass.trim() );
	}
	
	
	
	
	
}


