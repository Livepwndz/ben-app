package com.swifta.bj.mtn.fundamo.portal.controller.admin;

import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.swifta.bj.mtn.fundamo.portal.bean.BData;
import com.swifta.bj.mtn.fundamo.portal.bean.In;
import com.swifta.bj.mtn.fundamo.portal.bean.InSettings;
import com.swifta.bj.mtn.fundamo.portal.bean.InUserDetails;
import com.swifta.bj.mtn.fundamo.portal.bean.Out;
import com.swifta.bj.mtn.fundamo.portal.bean.OutProfile;
import com.swifta.bj.mtn.fundamo.portal.controller.main.DLoginUIController;
import com.swifta.bj.mtn.fundamo.portal.controller.util.RequiredTFValidator;
import com.swifta.bj.mtn.fundamo.portal.controller.util.TFNewPasswordValidator;
import com.swifta.bj.mtn.fundamo.portal.controller.util.UsernameTFValidator;
import com.swifta.bj.mtn.fundamo.portal.design.admin.DEditCredsUIDesign;
import com.swifta.bj.mtn.fundamo.portal.model.admin.MSettings;
import com.swifta.bj.mtn.fundamo.portal.model.admin.MUserDetails;
import com.swifta.bj.mtn.fundamo.portal.model.admin.MUserSelfCare;
import com.vaadin.data.Item;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.Validator;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.AbstractSelect.ItemCaptionMode;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

public class DUserEditCredsUI extends DEditCredsUIDesign implements
		DUIControllable {

	private static final long serialVersionUID = 1L;

	private Window processingPopup;
	private Logger log = LogManager.getLogger();
	private Item record;
	private ApplicationContext springAppContext;
	
	private String origUsername = null;
	
	

	public DUserEditCredsUI( Item record, ApplicationContext springAppContext ) {
		this.setSpringAppContext(springAppContext);
		this.setRecord( record );
		origUsername = record.getItemProperty( "username" ).getValue().toString();
		init();
	}
	
	

	public ApplicationContext getSpringAppContext() {
		return springAppContext;
	}



	public void setSpringAppContext(ApplicationContext springAppContext) {
		this.springAppContext = springAppContext;
	}



	public Window getProcessingPopup() {
		return processingPopup;
	}



	public void setProcessingPopup(Window processingPopup) {
		this.processingPopup = processingPopup;
	}



	@Override
	public void attachCommandListeners() {
		this.attachBtnCancel();
		this.attachOnClose();
		this.attachChkUsername();
		this.attachChkPass();
		this.attachChkProfile();
		
		//Fields
		this.attachBtnSave();
		this.attachTFNewPass();
	}
	
	
	
	
	private void attachBtnSave() {

		this.btnSave.addClickListener(new ClickListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {

				try {
					
					if( isFormValid() ){
						
						Out out = resetUserCreds();
						if( out.getStatusCode() == 1 ) {
						
							lbNormalMsg.removeStyleName("sn-display-none");
							lbErrorMsg.addStyleName("sn-display-none");
							Notification.show( out.getMsg(),
									Notification.Type.HUMANIZED_MESSAGE );
							
							tFCurrentPass.clear();
							tFNewUsername.clear();
							tFNewPass.clear();
							tFConfirmNewPass.clear();
							
							processingPopup.close();
							
						} else {
							
							lbErrorMsg.removeStyleName("sn-display-none");
							lbNormalMsg.addStyleName("sn-display-none");
							lbErrorMsg.setValue( out.getMsg() );
						}
				
					}
					
					recenterWindow();
				
				} catch ( Exception e){
					
					e.printStackTrace();
					
					Notification.show( "Error occured. Please try again / Contact support",
							Notification.Type.ERROR_MESSAGE );
					lbErrorMsg.removeStyleName("sn-display-none");
					lbNormalMsg.addStyleName("sn-display-none");
					lbErrorMsg.setValue( "Error occured. Please try again / Contact support"  );
				}
				

			}
			

		});

	}
	
	
	private boolean isFormValid(){
		

		if (chkUsername.getValue())
			if( !this.isUsernameTFValid( this.tFNewUsername ) )
				return false;

		//if( !this.isRequiredTFValid( this.tFCurrentPass ) )
			//return false;
		
		if (chkPass.getValue()) {
			
			tFNewPass.setComponentError( null );
			tFConfirmNewPass.setComponentError( null );
			if( !this.isPasswordTFValid( this.tFNewPass ) )
				return false;
			if( !this.isRequiredTFValid( this.tFConfirmNewPass ) )
				return false;
		} 
		
		return true;
	}
	
	
	private boolean isPasswordTFValid ( Field<?> tF ){
		
		
		
		if( !tF.isValid() ){
			
			lbErrorMsg.removeStyleName("sn-display-none");
			lbNormalMsg.addStyleName("sn-display-none");
			
			Iterator<Validator> itr = tF.getValidators().iterator();
			String msg = "";
			while( itr.hasNext() ){
				TFNewPasswordValidator v = (TFNewPasswordValidator) itr.next();
				msg += v.getErrorMessage();
			}
			
			lbErrorMsg.setValue( tF.getCaption()+" Error. "+msg );
			
			return false;
			
		} else {
			
			lbNormalMsg.removeStyleName("sn-display-none");
			lbErrorMsg.addStyleName("sn-display-none");
			
			
		}
		
		return true;
		
	}
	
	
	private boolean isUsernameTFValid ( Field<?> tF ){
		
		if( !tF.isValid() ){
			
			lbErrorMsg.removeStyleName("sn-display-none");
			lbNormalMsg.addStyleName("sn-display-none");
			
			Iterator<Validator> itr = tF.getValidators().iterator();
			String msg = "";
			while( itr.hasNext() ){
				UsernameTFValidator v = (UsernameTFValidator) itr.next();
				msg += v.getErrorMessage();
			}
			
			lbErrorMsg.setValue( tF.getCaption()+" Error. "+msg );
			
			return false;
			
		} else {
			
			lbNormalMsg.removeStyleName("sn-display-none");
			lbErrorMsg.addStyleName("sn-display-none");
			
			
		}
		
		return true;
		
	}
	
	
	private boolean isRequiredTFValid (Field<?> tF){
		
		if( !tF.isValid() ){
			
			Iterator<Validator> itr = tF.getValidators().iterator();
			String msg = "";
			while( itr.hasNext() ){
				RequiredTFValidator v = (RequiredTFValidator) itr.next();
				msg += v.getErrorMessage();
			}
			
			log.debug( "RT is invalid" );
			
			lbErrorMsg.setValue( tF.getCaption()+" Error. "+msg );
			lbErrorMsg.removeStyleName("sn-display-none");
			lbNormalMsg.addStyleName("sn-display-none");
			
			log.debug( "RT error set as: "+msg );
			
			return false;
			
		} else {
			
			lbNormalMsg.removeStyleName("sn-display-none");
			lbErrorMsg.addStyleName("sn-display-none");
			
			log.debug( "RT is valid" );
			
			
		}
		
		return true;
		
	}
	
	private void initChk(){
		
		this.chkUsername.setValue( true );
		
		tFNewPass.getParent().addStyleName("sn-display-none");
		tFConfirmNewPass.getParent().addStyleName("sn-display-none");
		comboProfile.getParent().addStyleName("sn-display-none");
		this.lbErrorMsg.addStyleName("sn-display-none");
		
	}
	
	private void initComboProfile(){
		
		comboProfile.setNullSelectionAllowed( false );
		BeanItemContainer<OutProfile> profiles = this.getProfiles();
		comboProfile.setContainerDataSource( profiles );
		comboProfile.setItemCaptionMode( ItemCaptionMode.PROPERTY );
		comboProfile.setItemCaptionPropertyId( "profileName" );
		
		Iterator< OutProfile > itr = profiles.getItemIds().iterator();
		while( itr.hasNext() ){
			
			OutProfile profile = itr.next();
			
			if( profile.getProfileId() == Integer.valueOf( record.getItemProperty( "profileId" ).getValue().toString() ) ) {
				comboProfile.setValue( profile );
				break;
			}
		}
		


	} //
	
	@SuppressWarnings("unchecked")
	private Out resetUserCreds(){
		
		
		// New username should be set to current username if username checkbox is not on
		if( !chkUsername.getValue() ){
			record.getItemProperty( "newUsername" ).setValue( getCurrentUsername() );
			record.getItemProperty( "username" ).setValue( getCurrentUsername() );			
		} else {
			record.getItemProperty( "username" ).setValue( record.getItemProperty( "newUsername" ).getValue().toString() );
		}
		
		// New password should be set to current password if password checkbox is not on
		if( !chkPass.getValue() ) {
			record.getItemProperty( "newPassword" ).setValue( record.getItemProperty( "password" ).getValue() );
		} else {
			record.getItemProperty( "newPassword" ).setValue( tFNewPass.getValue() );
		}
		
		
		

		log.info( "New password ds value: "+record.getItemProperty( "newPassword" ).getValue() );
		log.info( "New password ds value: "+this.tFNewPass.getValue() );
		log.info( "Comfirm password ds value: "+record.getItemProperty( "confirmPass" ).getValue() );
		log.info( "Current password ds value: "+record.getItemProperty( "password" ).getValue() );
		
		/*
		if( 1 == 1 )
			return new Out();*/
		
		InUserDetails inData = new InUserDetails();
		setAuth( inData );
		
		
		inData.setRecord( record );

		
		log.debug( "Current username: "+getCurrentUsername() );
		
		if( chkProfile.getValue() ) {
			OutProfile profile = (OutProfile) comboProfile.getValue();
			record.getItemProperty( "profileId" ).setValue( Integer.valueOf( profile.getProfileId() ) );
		}
	
		BData<InUserDetails> bData = new BData<>();
		bData.setData( inData );
		
		In in = new In();
		in.setData( bData );
		
		Out out = null;
		
		if(  Long.valueOf( record.getItemProperty( "userId" ).getValue().toString() ) != getCurrentUserId() ){
			 out = new MUserDetails(  getCurrentUserId(), getCurrentUserSession()  ).resetUserCreds( in );
		} else {
			 out = new MUserSelfCare().resetUserCreds( in );
		}
		
		tFCurrentPass.clear();

		if ( out.getStatusCode() != 1 ) {
			return out;
		}

		UI.getCurrent().getSession().setAttribute( DLoginUIController.USERNAMEx, inData.getUsername() );
		UI.getCurrent().getSession().setAttribute( DLoginUIController.SESSION_VAR,  inData.getUserSession() );
		// UI.getCurrent().getSession().setAttribute( DLoginUIController.PROFILE_ID, record.getItemProperty( "profileId" ).getValue() );
		
		if( chkProfile.getValue() ){
			initComboProfile();
		}
		
		
		tFCurrentPass.clear();
		tFNewUsername.clear();
		tFNewPass.clear();
		tFConfirmNewPass.clear();
		
		return out;


	}
	
	private BeanItemContainer< OutProfile > getProfiles(){
		

		MSettings mSettings = new MSettings(  getCurrentUserId(), getCurrentUserSession() );
		InSettings inData = new InSettings();
		setSettingsAuth( inData );
		
		inData.setProfileContainer( new BeanItemContainer<>( OutProfile.class) );
		
		BData< InSettings > bData = new BData<>();
		bData.setData( inData );
		
		In in = new In();
		in.setData( bData );
		
		Out out = mSettings.setProfiles(in );
		
		log.debug( "Set profiles msg: "+out.getMsg() );
		log.debug( "Set profiles status: "+out.getStatusCode() );
		
		return inData.getProfileContainer();
	}
	
	
	private void attachChkUsername() {
		tFNewUsername.clear();
		this.chkUsername.addValueChangeListener(new ValueChangeListener() {

			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {

				tFNewUsername.setComponentError(null);
				tFNewUsername.clear();
				
				Boolean state = (Boolean) event.getProperty().getValue();
				if (state) {
					tFNewUsername.getParent().removeStyleName("sn-display-none");
				} else {
					tFNewUsername.getParent().addStyleName("sn-display-none");
				}

				if (!chkUsername.getValue() && !chkPass.getValue() && !chkProfile.getValue()) {
					chkUsername.setValue(true);
				}
				
				recenterWindow();

			}

		});
	}
	
	private void attachChkPass() {
		this.chkPass.addValueChangeListener(new ValueChangeListener() {

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {
				
				tFCurrentPass.setComponentError(null);
				tFNewPass.setComponentError(null);
				tFConfirmNewPass.setComponentError(null);
				
				tFCurrentPass.clear();
				tFNewPass.clear();
				tFConfirmNewPass.clear();

				Boolean state = (Boolean) event.getProperty().getValue();
				if (state) {
					tFNewPass.getParent().removeStyleName("sn-display-none");
					tFConfirmNewPass.getParent()
							.removeStyleName("sn-display-none");
				} else {
					tFNewPass.getParent().addStyleName("sn-display-none");
					tFConfirmNewPass.getParent().addStyleName("sn-display-none");
				}


				if (!chkUsername.getValue() && !chkPass.getValue() && !chkProfile.getValue()) {
					chkUsername.setValue(true);
				}
				
				recenterWindow();

			}

		});
	}
	
	private void recenterWindow(){
		processingPopup.center();
	}
	
	private void attachChkProfile() {
		this.chkProfile.addValueChangeListener(new ValueChangeListener() {

			
			private static final long serialVersionUID = 1L;

			@Override
			public void valueChange(ValueChangeEvent event) {

				Boolean state = (Boolean) event.getProperty().getValue();
				if (state) {
					comboProfile.getParent().removeStyleName("sn-display-none");
					
				} else {
					comboProfile.getParent().addStyleName("sn-display-none");
					
				}


				if (!chkUsername.getValue() && !chkPass.getValue() && !chkProfile.getValue()) {
					chkUsername.setValue(true);
				}
				
				recenterWindow();

			}

		});
	}
	
	private void attachBtnCancel(){
		this.btnCancel.addClickListener( new ClickListener(){

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				processingPopup.close();
				
				
			}
			
		});
	}
	
	private void attachOnClose(){
		processingPopup.addCloseListener( new CloseListener(){

			private static final long serialVersionUID = 1L;

			@Override
			public void windowClose(CloseEvent e) {
				refreshRecord();
				new DUserDetailsUI( record, springAppContext );
				
			}
			
		});
	}
	
	
	private boolean refreshRecord(){
		
		
		
		
		if( record != null ) {
			
			InUserDetails inData = new InUserDetails();
			setAuth( inData );
			inData.setRecord( record );
			
			BData<InUserDetails> bData = new BData<>();
			bData.setData( inData );
			
			In in = new In();
			in.setData( bData );
			
			Out out = null;
			
			if( Long.valueOf( record.getItemProperty( "userId" ).getValue().toString() ) != getCurrentUserId() ){
				 out = new MUserDetails(  getCurrentUserId(), getCurrentUserSession()  ).setUserDetails(in );
			} else {
				 out = new MUserSelfCare().setUserDetailsx(in );
			}
			
			log.info( "Refresh message: "+out.getMsg(), this );
			return out.getStatusCode() == 1;
		
		} 
		
		return false;
		
		
	}
	
	public Item getRecord() {
		return record;
	}

	public void setRecord(Item record) {
		this.record = record;
	}


	private void init() {
		this.initChk();
		this.initComboProfile();
		this.setProcessingPopup( new Window("User Details") );
		attachCommandListeners();
		setPropertyDataSource();
		setContent();
		

	}
	
	private void attachTFNewPass(){
		// tFNewUsername.setInvalidCommitted( true );
		// tFCurrentPass.setInvalidCommitted( true );
		// tFNewPass.setInvalidCommitted( true );
		//tFNewUsername.setInvalidCommitted( true );
		this.tFNewPass.addValueChangeListener( ev -> { 
			tFNewPass.setComponentError( null );
			// log.info( "New pass value: "+ev.getProperty().getValue() );
			});
	}
	
	
	


	@SuppressWarnings("unchecked")
	private void setPropertyDataSource(){
		
		
		
		record.getItemProperty( "newUsername" ).setValue( "" );
		record.getItemProperty( "password" ).setValue( "" );
		record.getItemProperty( "newPassword" ).setValue( "" );
		
		// Username field
		UsernameTFValidator usernameTFValidator = new UsernameTFValidator( "Field equired in valid format" );
		usernameTFValidator.init(lbNormalMsg, lbErrorMsg,"" );
		this.tFNewUsername.addValidator( usernameTFValidator );
		//this.tFNewUsername.setInvalidCommitted( true );
		this.tFNewUsername.setPropertyDataSource( record.getItemProperty( "newUsername" ) );
		
		// Required fields
		// RequiredTFValidator rTFValidator = new RequiredTFValidator( "Field required in valid format" );
		// this.tFNewPass.addValidator(rTFValidator );
		//this.tFCurrentPass.addValidator(rTFValidator );
		// this.tFCurrentPass.setRequired( true );
		
		this.tFNewPass.setPropertyDataSource( record.getItemProperty( "newPassword" ) );
		this.tFCurrentPass.setPropertyDataSource( record.getItemProperty( "password" )  );
		
		
		// Password fields
		
		this.tFConfirmNewPass.setPropertyDataSource( record.getItemProperty( "confirmPass" ) );
		
		TFNewPasswordValidator newPassValidator = new TFNewPasswordValidator( "Field required in valid format." );
		tFNewPass.addValidator(  newPassValidator );
		newPassValidator.init( this.lbNormalMsg, this.lbErrorMsg, tFConfirmNewPass.getPropertyDataSource() );
		// this.tFConfirmNewPass.setRequired( true );
		
		// TFPasswordValidatorDefault passValidator = new TFPasswordValidatorDefault( "Field required. Password and Confirm Password fields should match." );
		// passValidator.init(lbNormalMsg, lbErrorMsg, record.getItemProperty( "newPassword" ) );
		
		
		
		
		lbErrorMsg.addStyleName("sn-display-none");
		lbNormalMsg.removeStyleName("sn-display-none");
		
		
		
	}
	
	private void format(){
		
		
	}
	

	
	
	private void showPopup() {
		processingPopup.setContent(this);
		processingPopup.center();
		processingPopup.setClosable( true );
		processingPopup.setEnabled(true);
		processingPopup.setModal(true);
		processingPopup.setDraggable(false);
		processingPopup.setResizable(false);
		processingPopup.setSizeUndefined();
		UI.getCurrent().addWindow(processingPopup);
	}


	private void setContent() {
		if(record != null  ) {
			showPopup();
			format();
		} else {
			
			Notification.show("Oops... error loading data. Please  try again.",
					Notification.Type.ERROR_MESSAGE);
		}
	}
	
	private void setAuth( InUserDetails inData ){
		
		inData.setUserId( Long.valueOf(UI.getCurrent().getSession().getAttribute( DLoginUIController.USER_ID).toString() ) );
		inData.setUsername( UI.getCurrent().getSession().getAttribute( DLoginUIController.USERNAMEx ).toString() );
		inData.setUserSession(  UI.getCurrent().getSession().getAttribute( DLoginUIController.SESSION_VAR ).toString()  );
	}
	
	private void setSettingsAuth( InSettings inData ){
		
		inData.setUsername( UI.getCurrent().getSession().getAttribute( DLoginUIController.USERNAMEx ).toString() );
		inData.setUserSession(  UI.getCurrent().getSession().getAttribute( DLoginUIController.SESSION_VAR ).toString()  );

		
	}
	
	private String getCurrentUsername(){
		return UI.getCurrent().getSession().getAttribute( DLoginUIController.USERNAMEx ).toString();
	}
	
	
	
	private long getCurrentUserId(){
		return ( long ) UI.getCurrent().getSession().getAttribute( DLoginUIController.USER_ID );
	}
	
	private String getCurrentUserSession(){
		return ( String ) UI.getCurrent().getSession().getAttribute( DLoginUIController.SESSION_VAR );
	}

}
