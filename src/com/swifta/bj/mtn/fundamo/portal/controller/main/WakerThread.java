package com.swifta.bj.mtn.fundamo.portal.controller.main;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

class WakerThread extends Thread {
	private static Logger log = LogManager.getLogger();
	private DelayObj delayObj;
	transient long timeout = 0;
	transient long timeoutInterval = 0;

	WakerThread(DelayObj delayObj, long timeout) {
		this.delayObj = delayObj;
		this.timeout = timeout;
		this.timeoutInterval = timeout/2;
	}

	

	private synchronized void waiter( long timeout ) throws InterruptedException {
		this.wait(timeout);
	}

	@Override
	public void run() {
		log.debug("Waker thread started...");
		try {

			while (timeout > 0) {
				log.debug("Waker thread loop started...");
				log.debug("Pre sleep timeout value: " + timeout + " ms(s) ");
				timeout = timeout - timeoutInterval;
				this.waiter( timeoutInterval );
				log.debug("Waker thread relooping [ after "+timeout+" ms(s)...");
				log.debug("Post sleep timeout value: " + timeout + " ms(s) ");
			}
			delayObj.setDelay(false);

		} catch (InterruptedException e) {
			e.printStackTrace();
			log.debug("Timeout thread has been interrupted. Re-running it...");
			run();
		}
	}
}