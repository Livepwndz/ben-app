package com.swifta.bj.mtn.fundamo.portal.controller.admin;

import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.swifta.bj.mtn.fundamo.portal.bean.AbstractDataBean;
import com.swifta.bj.mtn.fundamo.portal.bean.BData;
import com.swifta.bj.mtn.fundamo.portal.bean.In;
import com.swifta.bj.mtn.fundamo.portal.bean.InTxn;
import com.swifta.bj.mtn.fundamo.portal.bean.Out;
import com.swifta.bj.mtn.fundamo.portal.bean.OutTxnMeta;
import com.swifta.bj.mtn.fundamo.portal.controller.main.DLoginUIController;
import com.swifta.bj.mtn.fundamo.portal.controller.util.AbstractAllRowsActionsUI;
import com.swifta.bj.mtn.fundamo.portal.controller.util.MultiRowActionsUISub;
import com.swifta.bj.mtn.fundamo.portal.controller.util.PaginationUIController;
import com.swifta.bj.mtn.fundamo.portal.controller.util.RowActionsUISub;
import com.swifta.bj.mtn.fundamo.portal.controller.util.TextChangeListenerSub;
import com.swifta.bj.mtn.fundamo.portal.design.admin.DTxnStateUIDesign;
import com.swifta.bj.mtn.fundamo.portal.model.admin.IModel;
import com.vaadin.data.Item;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.GeneratedPropertyContainer;
import com.vaadin.data.util.ObjectProperty;
import com.vaadin.data.util.PropertyValueGenerator;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.FooterCell;
import com.vaadin.ui.Grid.FooterRow;
import com.vaadin.ui.Grid.HeaderCell;
import com.vaadin.ui.Grid.HeaderRow;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PopupView;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.datenhahn.vaadin.componentrenderer.ComponentRenderer;

public abstract class DTxnStateUI<R> extends DTxnStateUIDesign implements
		DUserUIInitializable<ISubUI, DTxnStateUI<R>>, DUIControllable {

	private static final long serialVersionUID = 1L;
	
	private ISubUI ancestor;
	protected Logger log = LogManager.getLogger(DTxnStateUI.class.getName());

	protected IModel<R> iModel;
	protected InTxn inTxn;
	protected Set< Short > permSet;
	

	private ApplicationContext springAppContext;

	DTxnStateUI( ISubUI a) {
		this(a.getSpringAppContext());
		this.setPermSet( a.getPermSet() );
		this.setiModel( a.getSpringAppContext() );
		init(a);
	}

	protected abstract IModel<R> getiModel( ApplicationContext cxt );
	
	
	private void setiModel( ApplicationContext cxt) {
		this.iModel = getiModel( cxt );
	}

	/*
	 * Shared constructor by both DTxnStateUI [ Parent class ] &
	 * DTxnStateUIArchive [ Child class ]. Note init() is not called in this. It
	 * only set's up data objects
	 */
	protected DTxnStateUI(ApplicationContext cxt) {
		this.setiModel( cxt );
		this.setSpringAppContext(cxt);
		this.setPermSet( null );
		inTxn = new InTxn();
		this.setInDate(inTxn, 1);
	}
	
	

	

	public Set<Short> getPermSet() {
		return permSet;
	}


	@SuppressWarnings("unchecked")
	public void setPermSet(Set<Short> permSet) {
		if( permSet == null )
			this.permSet = UI.getCurrent().getSession().getAttribute( Set.class );
		else
			this.permSet = permSet;
		
	}


	public ApplicationContext getSpringAppContext() {
		return springAppContext;
	}

	public void setSpringAppContext(ApplicationContext springAppContext) {
		this.springAppContext = springAppContext;
	}

	@Override
	public void attachCommandListeners() {

	}

	@Override
	public void setHeader() {
		this.lbDataTitle.setValue(" Transaction Records Today");
	}

	@Override
	public void setContent() {
		setHeader();
		setFooter();

		swap(this);
		attachCommandListeners();
		this.vlTrxnTable.addComponent(loadGridData(new BeanItemContainer<>(
				AbstractDataBean.class)));
		this.vlTrxnTable.setHeightUndefined();
		// this.vlTrxnTable.setWidth("1200px");
		this.vlTrxnTable.setWidth("100%");

	}

	@Override
	public void swap(Component cuid) {
		// ancestor.setHeight("100%");
		// cuid.setHeight("100%");

		// ancestor.addStyleName("sn-p");
		// cuid.addStyleName("sn-c");

		cuid.setHeight("100%");
		
		// TODO testing max content width
		cuid.setWidth( "100%" );
		((VerticalLayout)( (  Panel )cuid).getContent()).setWidth( "100%" );
		// VerticalLayout v = null;
		
		
		ancestor.getAncestorUI().getcMainContent().setHeight("100%");
		// ancestor.getAncestorUI().getcMainContent().setWidth( "100%" );
		ancestor.setHeight("100%");

		log.debug("Users height: " + cuid.getHeight());

		ancestor.swap(cuid);

	}

	@Override
	public void init(ISubUI a) {

		setAncestorUI(a);
		setContent();

	}

	@Override
	public void setFooter() {
		// TODO Auto-generated method stub

	}

	@Override
	public ISubUI getAncestorUI() {
		return ancestor;
	}

	@Override
	public void setAncestorUI(ISubUI a) {
		this.ancestor = a;

	}

	@Override
	public DTxnStateUI<R> getParentUI() {
		return this;
	}

	@Override
	public void setParentUI(DTxnStateUI<R> p) {
		// TODO Auto-generated method stub

	}

	protected Grid loadGridData(
			BeanItemContainer<AbstractDataBean> beanItemContainer) {
		
		Grid grid = new Grid();
		grid.addStyleName("sn-small-grid");
		grid.setSelectionMode(SelectionMode.MULTI);
		grid.setHeight("600px");
		grid.setWidth("100%");
		// grid.setWidthUndefined();
		
		try {
			
			log.debug("Locale: " + UI.getCurrent().getLocale());

			In in = new In();

			BData<InTxn> inBData = new BData<>();

			inTxn.setPage(1);
			inTxn.setPermSet( this.getPermSet() );
			
			// Set OutTxnMeta
			OutTxnMeta outTxnMeta = new OutTxnMeta();
			outTxnMeta
					.setTotalRevenue(new ObjectProperty<String>("0", String.class));
			outTxnMeta
					.setTotalRecord(new ObjectProperty<String>("0", String.class));
			inTxn.setMeta( outTxnMeta );
			
			
			
			// this.setInDate(inTxn, ( 365 * 3) );
			inBData.setData(inTxn);
			in.setData(inBData);

			// TODO validate response

			Out out = iModel.search(in, beanItemContainer);
			if (out.getStatusCode() != 1) {
				Notification.show(out.getMsg(),
						Notification.Type.WARNING_MESSAGE);
				return grid;
			} else {
				Notification.show(out.getMsg(),
						Notification.Type.HUMANIZED_MESSAGE);
			}

			// Add actions

			GeneratedPropertyContainer gpc = new GeneratedPropertyContainer(
					beanItemContainer);

			gpc.addGeneratedProperty("actions",
					new PropertyValueGenerator<Component>() {
						private static final long serialVersionUID = 1L;

						@Override
						public Component getValue(Item item, Object itemId,
								Object propertyId) {
							PopupView v = new PopupView("...",
									new RowActionsUISub(iModel, item));
							v.setWidth("100%");
							v.setHeight("100%");
							return v;
						}

						@Override
						public Class<Component> getType() {
							return Component.class;
						}

					});

			grid.setContainerDataSource(gpc);
			grid.getColumn("actions").setRenderer(new ComponentRenderer());
			
			grid.setColumnOrder("column1", "column2","column3","column4","column5","column6","column7","column8","column90","column91","column92",  "date", "actions");
			grid.setFrozenColumnCount(2);

			HeaderRow header = grid.prependHeaderRow();
			FooterRow footer = grid.prependFooterRow();
			HeaderRow headerTextFilter = grid.addHeaderRowAt(2);
			
			HeaderCell dateFilterCellH = header.join("column1", "column2","column3","column4","column5","column6","column7","column8","column90", "column91","column92", "date", "actions");
			
			PaginationUIController pageC = new PaginationUIController();
			AbstractAllRowsActionsUI<R, AbstractDataBean, TextChangeListenerSub< AbstractDataBean> > allRowsActionsUIH = getHeaderController(iModel,
					grid, in, pageC);
			
			
			dateFilterCellH.setComponent(allRowsActionsUIH);

			header.setStyleName("sn-date-filter-row");
			dateFilterCellH
					.setStyleName("sn-no-border-right sn-no-border-left");
			
			// Preparing footer
			FooterCell dateFilterCellF = footer.join("column1", "column2","column3","column4","column5","column6","column7","column8","column90","column91","column92","date", "actions");
			
			dateFilterCellF.setComponent(getFooterController(iModel, grid, in,
					pageC));

			// Initialize pagination controller after both header and footer have been set.
			pageC.init();

			footer.setStyleName("sn-date-filter-row");
			dateFilterCellF
					.setStyleName("sn-no-border-right sn-no-border-left");

			PopupView v = new PopupView("HHHH", null);
			v.setContent(new MultiRowActionsUISub(iModel, in, grid, v));
			v.setHideOnMouseOut(true);
			v.setVisible(true);

			HeaderCell cellBulkActions = headerTextFilter.getCell("actions");
			v.setWidth("100%");
			v.setHeight("100%");

			cellBulkActions.setComponent(v);

			grid.getColumn("actions").setWidth(50);
			HeaderRow headerColumnNames = grid.getHeaderRow(1);

			HeaderCell cellActions = headerColumnNames.getCell("actions");

			cellActions.setStyleName("sn-cell-actions");
			cellBulkActions.setStyleName("sn-cell-actions");
			

			// Add search field
			
			allRowsActionsUIH.prepareGridHeader(grid, "column1",
					"Transaction Number", true);
			allRowsActionsUIH.prepareGridHeader(grid, "column2", "Type", false);
			allRowsActionsUIH
					.prepareGridHeader(grid, "column3", "E. Type", false);
			allRowsActionsUIH
					.prepareGridHeader(grid, "column4", "Amount", false);
			allRowsActionsUIH.prepareGridHeader(grid, "column5", "Status", false );
			allRowsActionsUIH.prepareGridHeader(grid, "column6", "Payer MSISDN", true );
			allRowsActionsUIH.prepareGridHeader(grid, "column7", "Payee MSISDN", true );
			allRowsActionsUIH.prepareGridHeader(grid, "column8", "Payer Acc. No.", false );
			allRowsActionsUIH.prepareGridHeader(grid, "column90", "Payee Acc. No.", false );
			allRowsActionsUIH.prepareGridHeader(grid, "column91", "Payer Name", false );
			allRowsActionsUIH.prepareGridHeader(grid, "column92", "Payee Name", false );
			allRowsActionsUIH.prepareGridHeader(grid, "date", "Timestamp",
					false); 
			allRowsActionsUIH.prepareGridHeader(grid, "actions", "...", false);

			// Set column widths

			grid.getColumn("column1").setWidth(125);
			grid.getColumn("column2").setWidth( 200 ).setResizable(false);
			grid.getColumn("column3").setWidth(100);
			grid.getColumn("column5").setWidth( 200 ).setResizable(false);
			grid.getColumn("column6").setWidth( 200 ).setResizable(false);
			grid.getColumn("date").setWidth(178).setResizable(false);
			
			// grid.addStyleName( "sn-small-grid" );

			// DataExport dataExport = new DataExport();
			// dataExport.exportDataAsExcel( grid );
			
			// Hide unnecessary bean fields
			allRowsActionsUIH.removeUnnecessaryColumns(grid);

			return grid;

		} catch (Exception e) {

			Notification.show(
					"Error occured while loading data. Please try again!",
					Notification.Type.ERROR_MESSAGE);
			e.printStackTrace();

		}
		
		return grid;
	}

	
	/*
	protected AllRowsActionsUISub getHeaderController(IModel mSub, Grid grid,
			In in, PaginationUIController pageC) {
		return new AllRowsActionsUISub(mSub, grid, in, false, true, pageC);
		
		
	}*/
	
	
	protected abstract AbstractAllRowsActionsUI<R, AbstractDataBean, TextChangeListenerSub<AbstractDataBean> > getHeaderController(IModel<R> mSub, Grid grid,
			In in, PaginationUIController pageC);

	protected abstract AbstractAllRowsActionsUI<R, AbstractDataBean, TextChangeListenerSub<AbstractDataBean> > getFooterController(IModel<R> mSub, Grid grid,
			In in, PaginationUIController pageC);

	protected long getCurrentUserId() {
		return (long) UI.getCurrent().getSession()
				.getAttribute(DLoginUIController.USER_ID);
	}

	protected String getCurrentUserSession() {
		return (String) UI.getCurrent().getSession()
				.getAttribute(DLoginUIController.SESSION_VAR);
	}

	protected String getCurrentTimeCorrection() {
		return (String) UI.getCurrent().getSession()
				.getAttribute(DLoginUIController.TIME_CORRECTION);
	}

	protected void setInDate(InTxn inTxn, int dayOffSet) {

		inTxn.setfDate( "2010-02-01" );
		inTxn.settDate( "2010-02-08" );
		
		inTxn.setfDefaultDate( inTxn.getfDate() );
		inTxn.settDefaultDate( inTxn.gettDate() );

	}

}
