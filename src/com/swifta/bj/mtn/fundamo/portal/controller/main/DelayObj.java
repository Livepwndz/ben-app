package com.swifta.bj.mtn.fundamo.portal.controller.main;

public class DelayObj{
	
	private transient boolean isDelay = false;
	
	public synchronized void delay( DelayObj obj ) throws InterruptedException{
		while( isDelay ) {
			obj.wait();
		}
	}
	
	synchronized void stopDelay( DelayObj obj ) throws InterruptedException{
		obj.notify();
	}

	public boolean isDelay() {
		return isDelay;
	}

	public synchronized void setDelay(boolean isDelay) throws InterruptedException {
		this.isDelay = isDelay;
		if( !isDelay )
			stopDelay( this );
		
	}

	

	
	
	
	
	
	
}
