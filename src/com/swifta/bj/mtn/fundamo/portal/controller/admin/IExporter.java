package com.swifta.bj.mtn.fundamo.portal.controller.admin;

import com.vaadin.data.util.BeanItemContainer;

public interface IExporter< T > {
	BeanItemContainer< T > getExportData();
	void attachBtnXLS();
	void attachBtnCSV();
	void attachBtnPDF();

}
