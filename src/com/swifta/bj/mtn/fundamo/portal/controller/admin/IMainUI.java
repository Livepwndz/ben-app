package com.swifta.bj.mtn.fundamo.portal.controller.admin;

import com.vaadin.ui.VerticalLayout;

public interface IMainUI {
	ISubUI getAncestorUI();
	VerticalLayout getcMainContent();
}
