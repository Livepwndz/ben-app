package com.swifta.bj.mtn.fundamo.portal.controller.admin;

import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.swifta.bj.mtn.fundamo.portal.design.admin.DTxnUIDesign;
import com.swifta.bj.mtn.fundamo.portal.model.util.EnumPermission;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.Component;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

public class DLedgerUI extends DTxnUIDesign implements DUserUIInitializable<DMainUI,ISubUI>, ISubUI, DUIControllable {

	private static final long serialVersionUID = 1L;
	private DMainUI ancestor;
	private Logger log = LogManager.getLogger( );
	protected Set< Short > permSet;
	private ApplicationContext springAppContext;
	
	DLedgerUI(DMainUI a) {
		this.setPermSet( a.getPermSet() );
		this.setSpringAppContext( a.getSpringAppContext() );
		this.init(a);
		this.btnDay.setIcon( FontAwesome.BRIEFCASE );
		this.btnDay.setCaption( "Ledger" );
		this.btnArchive.setVisible( false );
		
		
	}

	@Override
	public void setContent() {
		setHeader();
		setFooter();
		// swap( new DTxnStateUI( getParentUI() ) ); 
		log.info( "Ledger, perm count: "+permSet.size() );
		if( permSet.contains( EnumPermission.REPORT_VIEW_LEDGER.val ) ){
			swap( new DTxnStateLedgerArchiveUI( getParentUI() ) );
			attachCommandListeners();
		} else {
			VerticalLayout c = new VerticalLayout();
			c.setWidth( "100%" );
			c.setHeight( "100%" );
			swap( c );
		}
	}

	
	

	public ApplicationContext getSpringAppContext() {
		return springAppContext;
	}




	public void setSpringAppContext(ApplicationContext springAppContext) {
		this.springAppContext = springAppContext;
	}


	

	@Override
	public void setHeader() {
		// TODO Auto-generated method stub
		
	}



	@Override
	public void swap(Component cuid) {
		this.cForms.replaceComponent(cForms.getComponent( 0 ), cuid);
		log.debug( "SUB-UI is swapped." );
		
	}

	@Override
	public void init(DMainUI a) {
		setAncestorUI( a );
		setContent();
		
	}

	@Override
	public void setFooter() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public DMainUI getAncestorUI() {
		return ancestor;
	}

	@Override
	public void setAncestorUI(DMainUI a) {
		this.ancestor = a;
		
	}

	@Override
	public ISubUI getParentUI() {
		return this;
	}

	@Override
	public void setParentUI(ISubUI p) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public VerticalLayout getcMainContent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Set<Short> getPermSet() {
		return permSet;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void setPermSet(Set<Short> permSet) {
		if( permSet == null )
			this.permSet = UI.getCurrent().getSession().getAttribute( Set.class );
		else
			this.permSet = permSet;
		
	}


	@Override
	public void attachCommandListeners() {
		// TODO Auto-generated method stub
		
	}
	

}