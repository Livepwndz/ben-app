package com.swifta.bj.mtn.fundamo.portal.controller.admin;

import org.springframework.context.ApplicationContext;

import com.swifta.bj.mtn.fundamo.portal.bean.AbstractDataBean;
import com.swifta.bj.mtn.fundamo.portal.bean.In;
import com.swifta.bj.mtn.fundamo.portal.controller.util.AbstractAllRowsActionsUI;
import com.swifta.bj.mtn.fundamo.portal.controller.util.AllRowsActionsUISub;
import com.swifta.bj.mtn.fundamo.portal.controller.util.PaginationUIController;
import com.swifta.bj.mtn.fundamo.portal.controller.util.TextChangeListenerSub;
import com.swifta.bj.mtn.fundamo.portal.model.admin.IModel;
import com.swifta.bj.mtn.fundamo.portal.model.admin.IModelSpring;
import com.swifta.bj.mtn.fundamo.portal.model.admin.MSub;
import com.swifta.bj.mtn.fundamo.portal.model.admin.Person;
import com.swifta.bj.mtn.fundamo.portal.spring.fundamo.repo.Transaction001Repo;
import com.vaadin.ui.Grid;

public class DTxnStateArchiveUI extends DTxnStateUI<Transaction001Repo> {
	private static final long serialVersionUID = 1L;
	
	DTxnStateArchiveUI( ISubUI a ){
		super( a.getSpringAppContext() );
		this.setInDate( inTxn, 4*365 );
		this.init(a);
		log.debug( "Archive UI loaded successfully." );
	}
	

	@Override
	protected IModelSpring<Transaction001Repo> getiModel( ApplicationContext cxt ) {
		
		@SuppressWarnings("unchecked")
		IModelSpring< Transaction001Repo > mSub = (IModelSpring<Transaction001Repo>) cxt.getBean( "mSub" );
		mSub.setUserAuthId( getCurrentUserId() );
		mSub.setUserAuthSession( this.getCurrentUserSession() );
		return mSub;
	}
	
	
	/*
	 * This is for sub classes, not to call init.
	 */
	protected DTxnStateArchiveUI( ApplicationContext cxt ){
		super( cxt );
		this.setInDate( inTxn, 4*365 );
	}

	@Override
	public void setHeader() {
		this.lbDataTitle.setValue("Subscriber Transaction Archive");
	}
	
	@Override
	protected AbstractAllRowsActionsUI< Transaction001Repo, AbstractDataBean, TextChangeListenerSub<AbstractDataBean> > getHeaderController( IModel<Transaction001Repo> mSub, Grid grid, In in, PaginationUIController pageC ){
		return new AllRowsActionsUISub( mSub, grid, in, true, true, pageC );
	}
	
	@Override
	protected AbstractAllRowsActionsUI< Transaction001Repo, AbstractDataBean, TextChangeListenerSub<AbstractDataBean> > getFooterController( IModel< Transaction001Repo > mSub, Grid grid, In in, PaginationUIController pageC ){
		return new AllRowsActionsUISub( mSub, grid, in, false, false, pageC );
	}


}
