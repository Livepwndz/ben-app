package com.swifta.bj.mtn.fundamo.portal.controller.main;

import com.swifta.bj.mtn.fundamo.portal.bean.Out;
import com.swifta.bj.mtn.fundamo.portal.model.main.MUserSelfCare;
import com.vaadin.data.Item;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.ui.Label;


public class EmailValidatorCustom extends EmailValidator{

	private static final long serialVersionUID = 1L;
	
	
	private boolean isInitCalled = false;
	private Label lbNormalMsg;
	private Label lbErrorMsg;
	private Item record;

	public EmailValidatorCustom(String errorMessage, Item record ) {
		super(errorMessage);
		this.record = record;
		
	}
	
	public void init( Label lbNormal, Label lbError, String newEmail ){
		this.lbErrorMsg = lbError;
		this.lbNormalMsg = lbNormal;
		isInitCalled = true;
	}
	
	
	
	@Override
	protected boolean isValidValue(String value) {
		
		boolean status = super.isValidValue(value);
		
		if( status ){
			Out out = new MUserSelfCare().checkEmail( value, record );;
			status =  out.getStatusCode() == 1;
			
			if( status ){
				if( isInitCalled ){
					lbNormalMsg.removeStyleName("sn-display-none");
					lbErrorMsg.addStyleName("sn-display-none");
				}
			} else {
				if( isInitCalled ){
					
					lbErrorMsg.removeStyleName("sn-display-none");
					lbNormalMsg.addStyleName("sn-display-none");
					lbErrorMsg.setValue( out.getMsg() );
				}
				this.setErrorMessage( out.getMsg() );
			}
		}
		
		
		
		return status;
		
	}
	
	

	
	
}
