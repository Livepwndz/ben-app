package com.swifta.bj.mtn.fundamo.portal.controller.main;

import javax.servlet.annotation.WebListener;
import javax.servlet.annotation.WebServlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.web.context.ContextLoaderListener;

import com.swifta.bj.mtn.fundamo.portal.controller.admin.DMainUI;
import com.swifta.bj.mtn.fundamo.portal.model.admin.IModelSpring;
import com.swifta.bj.mtn.fundamo.portal.model.admin.Person;
import com.swifta.bj.mtn.fundamo.portal.spring.config.Config;
import com.swifta.bj.mtn.fundamo.portal.spring.config.DataAccessConfigUser;
import com.swifta.bj.mtn.fundamo.portal.spring.config.JpaConfig;
import com.vaadin.annotations.Push;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.shared.communication.PushMode;
import com.vaadin.spring.annotation.EnableVaadin;
import com.vaadin.spring.annotation.SpringUI;
// import com.vaadin.server.VaadinServlet;
import com.vaadin.spring.server.SpringVaadinServlet;
import com.vaadin.ui.UI;

@SpringUI
@SuppressWarnings("serial")
@Theme("portal_theme")
@Push( PushMode.AUTOMATIC )
@Title( "MTN Benin Reports Portal" )
public class PortalUI extends UI {
	
	@Configuration
	@EnableVaadin
	@ContextConfiguration( classes = { Config.class, DataAccessConfigUser.class, JpaConfig.class } )
	public static class VaadinSpringConfig {

	}
	
	private static Logger log = LogManager.getLogger( PortalUI.class );
	@Autowired
	private Person person;
	
	// @Autowired
	// private IModel< Transaction001Repo > mSub;
	
	@Autowired
	private ApplicationContext cxt;
	
	
	@Autowired
	private DMainUI defaultUI;
	
	@Autowired
	private DLoginUIController loginUIController;
	
	


	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = true, ui = PortalUI.class, widgetset = "com.swifta.bj.mtn.fundamo.portal.controller.main.widgetset.Mtnb_portalWidgetset")
	
	/*
	 * Instead of VaadinServlet, extend SpringVaadinServlet
	 */
	public static class UIServlet extends SpringVaadinServlet {
	}
	
	/*
	 * Let spring create & load the context with UI injected
	 */
	@WebListener
    public static class MyContextLoaderListener extends ContextLoaderListener {
    }

	@Override
	
	protected void init(VaadinRequest request) {
		
		
		// Central error handling.
		setErrorHandler( new PortalUIErrorHandler() );
		// UI.getCurrent().setErrorHandler( );
		// VaadinSession.getCurrent().setErrorHandler(  new PortalErrorHandler() );
		
		Navigator nav = UI.getCurrent().getNavigator();
		if ( nav == null )
			nav = new Navigator(this, this);

		// DUI dui = new DUI();
		// dui.init(dui);
		// nav.addView("", dui);
		
		// DMainUI defaultUI = new DMainUI();
		
		
		nav.addView("", defaultUI );
		nav.addView("login", loginUIController );
		//nav.addView("management", DManUIController.class);
		nav.addView("management", defaultUI );
		
		log.debug( person.greet(), this );
		
		
		
		IModelSpring< ? > mSub = (IModelSpring<?>) cxt.getBean( "mSub" );
		if( mSub == null )
			log.error( person.greet()+" - mSub NOT set", this );
		else
			log.info( person.greet()+" - mSub set", this ); 

	}
	

	


}