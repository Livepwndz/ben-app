package com.swifta.bj.mtn.fundamo.portal.controller.main;

import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;

import com.swifta.bj.mtn.fundamo.portal.bean.BData;
import com.swifta.bj.mtn.fundamo.portal.bean.In;
import com.swifta.bj.mtn.fundamo.portal.bean.InUserDetails;
import com.swifta.bj.mtn.fundamo.portal.bean.Out;
import com.swifta.bj.mtn.fundamo.portal.controller.main.EmailValidatorCustom;
import com.swifta.bj.mtn.fundamo.portal.design.admin.DRecoverCredsUIDesign;
import com.swifta.bj.mtn.fundamo.portal.model.main.MUserSelfCare;
import com.swifta.bj.mtn.fundamo.portal.model.main.MUtil;
import com.swifta.bj.mtn.fundamo.portal.spring.email.EmailTemplate;
import com.vaadin.data.Item;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Window;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

public class DUserRecoverCredsUI extends DRecoverCredsUIDesign implements
		DUIControllable {

	private static final long serialVersionUID = 1L;

	private Window processingPopup;
	private Logger log = LogManager.getLogger( DUserRecoverCredsUI.class.getName() );
	private Item record;
	private ApplicationContext springAppContext;
	

	public DUserRecoverCredsUI( Item record, ApplicationContext springAppContext ) {
		this.setSpringAppContext(springAppContext);
		this.setRecord( record );
		init();
	}
	
	

	public ApplicationContext getSpringAppContext() {
		return springAppContext;
	}



	public void setSpringAppContext(ApplicationContext springAppContext) {
		this.springAppContext = springAppContext;
	}



	public Window getProcessingPopup() {
		return processingPopup;
	}



	public void setProcessingPopup(Window processingPopup) {
		this.processingPopup = processingPopup;
	}



	@Override
	public void attachCommandListeners() {
		this.attachBtnCancel();
		this.attachOnClose();
		this.attachBtnGenNewPass();
		this.attachBtnSave();
	}
	
	
	
	private void attachBtnSave(){
		this.btnSave.addClickListener( new ClickListener(){

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				
				try {
					if( isFormValid() ){
						
						// Capture values for email before data reset call
						
						tFNewPass.setValue( MUtil.genNewPass() );
						String username = record.getItemProperty( "username" ).getValue().toString();
						String password = record.getItemProperty( "newPassword" ).getValue().toString();
						
						// log.info( "Password: "+password );
						
						String email = record.getItemProperty( "email" ).getValue().toString();
						
						Out out = resetUserCreds( email );
						if( out.getStatusCode() == 1 ){
							
							// Send email
							boolean emailSent = emailSent( username, password, email );
							log.info("Is email sent?: " + emailSent);
							if ( emailSent ) {
								showSuccess("Recovery email notification sent. Check your inbox for reset instructions");
								processingPopup.close();
								
								
								
							} else {
								showWarm("Email notification could not be sent. Try again / Contact support");
								
							}
							
							
							/*Notification.show( out.getMsg(),
									Notification.Type.HUMANIZED_MESSAGE );*/
							
						}else{
							showWarm( out.getMsg() );
						}
					}
					} catch ( Exception e ){
						
						e.printStackTrace();
						String msg = "Error occured. Please try again / Contact support.";
						showWarm( msg );
						
					}
				
				
			}
			
		});
	}
	
	private void showSuccess( String msg ){
		
		Notification.show( msg,
				Notification.Type.HUMANIZED_MESSAGE );
		lbNormalMsg.removeStyleName("sn-display-none");
		lbErrorMsg.addStyleName("sn-display-none");
		lbNormalMsg.setValue( msg );
		this.tFRecoveryEmail.clear();
		this.tFNewPass.clear();
		
	}
	
	private void showWarm( String msg ){
		
		lbErrorMsg.removeStyleName("sn-display-none");
		lbNormalMsg.addStyleName("sn-display-none");
		lbErrorMsg.setValue( msg );
		this.tFRecoveryEmail.clear();
			
	}

	
	
	
	private boolean emailSent( String username, String password, String email ) {

		EmailTemplate emailTemplate = springAppContext
				.getBean(EmailTemplate.class);
		return emailTemplate.sendCredentials( username, password, email );

	}
	
	
	private Out resetUserCreds( String email ) throws Exception{
		
		// Check last update
		
		Calendar cal = Calendar.getInstance();
		
		Object objLastLogin = record.getItemProperty( "lastLogin" ).getValue();
		Object resetLoginSession = record.getItemProperty( "resetLoginSession" ).getValue();
		Date possibleResetDate = new Date();
		if( objLastLogin != null )
			possibleResetDate = ( Date ) objLastLogin;
		
		
		cal.setTime(possibleResetDate);
		cal.add(Calendar.MINUTE, 30 );
		possibleResetDate = cal.getTime();

		Date now = new Date();
		log.debug("Password reset time limit:" + possibleResetDate.toString());
		log.debug("Current timestamp:" + now.toString());

		// Check password reset timeout only if previous session is null
		// [ This is only on user registration, password recovery & administrator password reset ]
		if (resetLoginSession  == null
				&& possibleResetDate.compareTo(now) > 0) {
			Out out = new Out();
			out.setMsg( "Recovery window closed. Await 30 min. to try again" );
			return out;
		}
		
		
		
		InUserDetails inData = new InUserDetails();
		inData.setRecord( record );

	
		BData<InUserDetails> bData = new BData<>();
		bData.setData( inData );
		
		In in = new In();
		in.setData( bData );
		
		Out out = null;
		
		long timeoutLowerLimit = new Date().getTime();
		long defaultTimeout = 10000; // 20 seconds 

		out = new MUserSelfCare().resetUserCreds( email, in );
		
		
		long timeoutUpperLimit = new Date().getTime();
		long timeout = defaultTimeout - ( timeoutUpperLimit - timeoutLowerLimit );
		
		/*
		log.debug( "Default timeout: "+defaultTimeout );
		log.debug( "Timeout lower limit: "+timeoutLowerLimit );
		log.debug( "Timeout upper limit: "+timeoutUpperLimit );
		log.debug( "Effected timeout: "+timeout );*/
		
		// Timer attack snippet
		DelayObj delayObj = new DelayObj();
		delayObj.setDelay(true);
		new WakerThread(delayObj, timeout ).start();
		log.debug("Delay started...");
		delayObj.delay(delayObj);
		log.debug("Delay complete.");

		

		if ( out.getStatusCode() != 1 ) {
			return out;
		}
		
		
		return out;


	}
	
	
	private void attachBtnGenNewPass(){
		this.btnGenNewPass.addClickListener( new ClickListener(){

			private static final long serialVersionUID = 1L;

			@SuppressWarnings("unchecked")
			@Override
			public void buttonClick(ClickEvent event) {
				
				tFNewPass.setReadOnly( false );
				record.getItemProperty( "newPassword" ).setValue( MUtil.genNewPass() );
				tFNewPass.setReadOnly( true );
				
				
			}
			
		});
	}
	
	
	
	
	private boolean isFormValid(){
		return this.tFRecoveryEmail.isValid();
	}
	
	private void attachBtnCancel(){
		this.btnCancel.addClickListener( new ClickListener(){

			private static final long serialVersionUID = 1L;

			@Override
			public void buttonClick(ClickEvent event) {
				processingPopup.close();
			}
			
		});
	}
	
	private void attachOnClose(){
		processingPopup.addCloseListener( new CloseListener(){

			private static final long serialVersionUID = 1L;

			@Override
			public void windowClose(CloseEvent e) {
				
			}
			
		});
	}
	
	

	
	public Item getRecord() {
		return record;
	}

	public void setRecord(Item record) {
		this.record = record;
	}

	public void init() {
	
		this.setProcessingPopup( new Window("Password Recovery") );
		attachCommandListeners();
		setPropertyDataSource();
		setContent();
		

	}
	


	@SuppressWarnings("unchecked")
	private void setPropertyDataSource(){
		
		lbNormalMsg.removeStyleName("sn-display-none");
		lbErrorMsg.addStyleName("sn-display-none");
		
		this.tFRecoveryEmail.setRequired( true );
		this.tFNewPass.setPropertyDataSource( record.getItemProperty( "newPassword" ) );
		this.tFRecoveryEmail.setPropertyDataSource( record.getItemProperty( "email" ) );
		
		record.getItemProperty( "newPassword" ).setValue( MUtil.genNewPass() );
		
		
		// Email
		EmailValidatorCustom emailValidator = new EmailValidatorCustom( "Correct email format. [ i.e. example@domain.com ]", record );
		
		emailValidator.init(lbNormalMsg, lbErrorMsg, record.getItemProperty( "email" ).getValue().toString() );
		this.tFRecoveryEmail.addValidator( emailValidator );
				
		
		log.debug( "Field data sources have been initialized successfully." );
		
	

		
	}
	
	private void format(){
		
		
	}
	

	
	
	private void showPopup() {
		processingPopup.setContent(this);
		processingPopup.center();
		processingPopup.setClosable( true );
		processingPopup.setEnabled(true);
		processingPopup.setModal(true);
		processingPopup.setDraggable(false);
		processingPopup.setResizable(false);
		processingPopup.setSizeUndefined();
		UI.getCurrent().addWindow(processingPopup);
	}


	private void setContent() {
		if(record != null  ) {
			showPopup();
			format();
		} else {
			
			Notification.show("Oops... error loading data. Please  try again.",
					Notification.Type.ERROR_MESSAGE);
		}
	}
	

	



	@Override
	public void setUIState() {
		// TODO Auto-generated method stub
		
	}
	


}
