package com.swifta.bj.mtn.fundamo.portal.controller.main;

import java.io.Serializable;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.vaadin.server.DefaultErrorHandler;
import com.vaadin.server.ErrorHandler;
import com.vaadin.server.ErrorMessage;
import com.vaadin.server.Page;
import com.vaadin.server.UserError;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

// Custom error handler implementing class
public class PortalUIErrorHandler implements ErrorHandler, Serializable {
	private static final long serialVersionUID = 4956164168881514036L;

	private Logger log = LogManager.getLogger();
	@Override
	public void error(com.vaadin.server.ErrorEvent event) {
		log.info( "UI error: "+event.getThrowable().getMessage() );
		event.getThrowable().printStackTrace();
		AbstractComponent component = DefaultErrorHandler.findAbstractComponent(event);
	    if (component != null) {
	        ErrorMessage errorMessage = new UserError( "Oops... request could not be completed." );
	        if (errorMessage != null) {
	           component.setComponentError(errorMessage);
	            new Notification(null, errorMessage.getFormattedHtmlMessage(), Type.WARNING_MESSAGE, true).show(Page.getCurrent());
	            return;
	        }
	   }
	  DefaultErrorHandler.doDefault(event);
	  
	    
	    
		
	}
	
	
	
}